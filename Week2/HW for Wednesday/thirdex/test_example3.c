#include <stdio.h>
#include "../minunit.h"
#include "thirdex.c"

int tests_run = 0;

int location =0;
int a[] = {1, 2, 3, 4, 5};


static char * test_example1_01(){
    location = 0;
    mu_assert("error, first ([1,2,3,4,5], 5) != 1,2,3,4,5", thirdex(a, location) == 1);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_example1_01);
    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else{
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run  : %d\n", tests_run);

    return result != 0;
}