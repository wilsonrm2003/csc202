#include <stdio.h>
#include "../minunit.h"
#include "fourthex.c"

int tests_run = 0;

int length = 0;
int a[] = {2, 2, 3, 4, 5};


static char * test_example1_01(){
    length = 5;
    mu_assert("\n error, first ([2,2,3,4,5], 5) != 4", fourthex(a, length) == 4);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_example1_01);
    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else{
        printf("\n ALL TESTS PASSED\n");
    }
    printf("Tests run  : %d\n", tests_run);

    return result != 0;
}