#include <stdio.h>
int fourthex(int arr[], int n) {
    int i, large, second_large;

    large = arr[0];
    for(i=1; i<n; i++) {
        if(arr[i]>large){
            large = arr[i];
        }
    }
    second_large = arr[1];
    for(i=0; i<n; i++) {
        if(arr[i] != large){
            if(arr[i] > second_large)
                second_large = arr[i];
        }
    }
    printf("second largest %d", second_large);
    printf("largest %d", large);
    return second_large;
}