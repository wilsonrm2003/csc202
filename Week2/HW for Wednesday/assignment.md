Use the TDD approach we discussed in class to write functions to implement each of the algorithms presented in the first 5 example programs from Chapter 3: Arrays (pages 72 to 75).

To earn an A for this assignment there should be evidence in you git commit history of each stage in the development process, similar to the example presented in class.