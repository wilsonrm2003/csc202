#include <stdio.h>
#include "../minunit.h"
#include "firstex.c"

int tests_run = 0;

int a[] = {1, 2, 3, 4, 5};
int b[] = {1, 2, 3, 4, 5, 6, 7, 8};

static char * test_example1_01(){
    mu_assert("error, first ([1,2,3,4,5], 5) != 1,2,3,4,5", firstex(a, 5) == *a);
    return 0;
}

static char * test_example1_02(){
    mu_assert("error, second ([1,2,3,4,5,6,7,8], 8) != 1,2,3,4,5,6,7,8", firstex(b, 8) == *a);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_example1_01);
    mu_run_test(test_example1_02);
    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else{
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run  : %d\n", tests_run);

    return result != 0;
}