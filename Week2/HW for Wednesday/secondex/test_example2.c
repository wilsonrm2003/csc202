#include <stdio.h>
#include "../minunit.h"
#include "secondex.c"

int tests_run = 0;

int length = 0;
int a[] = {1, 2, 3, 4, 5};
int b[] = {4,5,6,9};
int c[] = {6,6};


static char * test_example1_01(){
    length = 5;
    mu_assert("error, first ([1,2,3,4,5], 5) != 1,2,3,4,5", secondex(a, length) == 3.00);
    return 0;
}

static char * test_example1_02(){
    length = 4;
    mu_assert("error, second ([4,5,6,9], 4) != 4,5,6,9", secondex(b, length) == 6.00);
    return 0;
}

static char * test_example1_03(){
    length = 2;
    mu_assert("error, third ([6,6], 2) != 6,6", secondex(c, length) == 6.00);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_example1_01);
    mu_run_test(test_example1_02);
    mu_run_test(test_example1_03);
    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else{
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run  : %d\n", tests_run);

    return result != 0;
}