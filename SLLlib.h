#include <stdio.h>
#include <stdlib.h>

struct node {
    int data;
    struct node *next;
};

/*
struct node *start = NULL;

struct node *create_ll(struct node *start, int data[]);
struct node *display(struct node *start);
struct node *insert_beg(struct node *start, int num);
struct node *insert_end(struct node *start, int num);
struct node *insert_before(struct node *start, int val_b, int num_b);
struct node *insert_after(struct node *start, int num_a, int val_a);
struct node *delete_beg(struct node *start);
struct node *delete_end(struct node *start);
struct node *delete_node(struct node *start, int val);
struct node *delete_after(struct node *start, int val);
struct node *delete_list(struct node *start);
struct node *sort_list(struct node *start);
*/

struct node *create_ll(struct node *start, int data[]) {
    struct node *new_node, *ptr;
    int i = 0, num;
    while(num!=-1) {
        num = data[i];
        new_node = (struct node*)malloc(sizeof(struct node));
        new_node->data = num;
        if (start==NULL) {
            new_node->next = NULL;
            start = new_node;
        } else {
            ptr = start;
            while (ptr->next != NULL) 
            ptr = ptr->next;
            ptr->next = new_node;
            new_node->next = NULL;
        }
        i++;
        num = data[i];
    }
    return start;
}

struct node *display(struct node *start) {
    struct node *ptr;
    ptr = start;
    while (ptr != NULL) {
        printf(" %d", ptr->data);
        ptr = ptr->next;
    }
    printf("\n");
    return start;
}

struct node *insert_beg(struct node *start, int num) {
    struct node *new_node;
    new_node = (struct node*)malloc(sizeof(struct node));
    new_node->data = num;
    new_node->next = start;
    start = new_node;
    return start;
}

struct node *insert_end(struct node *start, int num) {
    struct node *ptr, *new_node;
    new_node = (struct node *)malloc(sizeof(struct node));
    new_node -> data = num;
    new_node -> next = NULL;
    ptr = start;
    while(ptr -> next != NULL)
    ptr = ptr -> next;
    ptr -> next = new_node;
    return start;
}

struct node *insert_before(struct node *start, int num_b, int val_b) {
    struct node *new_node, *ptr, *preptr;
    int num = num_b, val = val_b;
    new_node = (struct node *)malloc(sizeof(struct node));
    new_node -> data = num;
    ptr = start;
    while(ptr -> data != val) {
        preptr = ptr;
        ptr = ptr -> next;
    }
    preptr -> next = new_node;
    new_node -> next = ptr;
    return start;
}

struct node *insert_after(struct node *start, int num_a, int val_a) {
    struct node *new_node, *ptr, *preptr;
    int num = num_a, val = val_a;
    new_node = (struct node *)malloc(sizeof(struct node));
    new_node -> data = num;
    ptr = start;
    preptr = ptr;
    while(preptr -> data != val) {
        preptr = ptr;
        ptr = ptr -> next;
    }
    
    preptr -> next=new_node;
    new_node -> next = ptr;
    return start;
}

struct node *delete_beg(struct node *start) {
    struct node *ptr;
    ptr = start;
    start = start -> next;
    free(ptr);
    return start;
}

struct node *delete_end(struct node *start) {
    struct node *ptr, *preptr;
    ptr = start;
    while(ptr -> next != NULL)
    {
        preptr = ptr;
        ptr = ptr -> next;
    }
    preptr -> next = NULL;
    free(ptr);
    return start;
}

struct node *delete_node(struct node *start, int val) {
    struct node *ptr, *preptr;
    ptr = start;
    if(ptr -> data == val) {
        start = delete_beg(start);
        return start;
    }
    else {
    while(ptr -> data != val) {
        preptr = ptr;
        ptr = ptr -> next;
    }
    preptr -> next = ptr -> next;
    free(ptr);
    return start;
    }
}

struct node *delete_after(struct node *start, int val) {
    struct node *ptr, *preptr;
    ptr = start;
    preptr = ptr;
    while(preptr -> data != val) {
        preptr = ptr;
        ptr = ptr -> next;
    }
    preptr -> next=ptr -> next;
    free(ptr);
    return start;
}

struct node *delete_list(struct node *start) {
	struct node *ptr;
    if(start!=NULL){
        ptr=start;
        while(ptr != NULL) {
            printf("\n %d is to be deleted next", ptr -> data);
            start = delete_beg(ptr);
            ptr = start;
        }
    }
    return start;
}

struct node *sort_list(struct node *start) {
    struct node *ptr1, *ptr2;
    int temp;
    ptr1 = start;
    while(ptr1 -> next != NULL) {
        ptr2 = ptr1 -> next;
        while(ptr2 != NULL) {
            if(ptr1 -> data > ptr2 -> data) {
                temp = ptr1 -> data;
                ptr1 -> data = ptr2 -> data;
                ptr2 -> data = temp;
            }
            ptr2 = ptr2 -> next;
        }
        ptr1 = ptr1 -> next;
    }
    return start;
}

int num_nodes(struct node *start) {
    struct node *ptr;
    int total_nodes = 0;
    ptr = start;
    while (ptr->next != NULL) {
        total_nodes++;
        ptr = ptr->next;
    }
    return total_nodes;
}

int node_data(struct node *start, int node_num) {
    struct node *ptr;
    int curr_data, loop = 0;
    ptr = start;
    while (loop != -1) {
        curr_data = ptr->data;
        ptr = ptr->next;
        if (curr_data == node_num) {
            break;;
        }
    }
    return curr_data;
}