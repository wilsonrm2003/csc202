#include <stdio.h>

int Lambda(int); // declare func
int main() {
    int num, val;
    printf("\nEnter number: ");
    scanf("%d", &num);
    val = Lambda(num);
    printf("\n Lambda of %d = %d", num, val);
}

int Lambda(int n){
    if (n == 1)
        return 0;
    else if (n > 1)
        return (1 + Lambda(n/2));
    else
        return -1;
}