#include <stdio.h>

int F(int, int);
int main(){
    int num1, num2, res;
    printf("enter 2 numbers: ");
    scanf("%d %d", &num1, &num2);
    res = F(num1, num2);
    printf("\n F of %d and %d = %d", num1, num2, res);
    return 0;
}

int F(int M, int N){
    if (M == 0)
        return 1;
    else if ( M >= N >= 1)
        return 1;
    else 
        return (F(M-1, N) + F(M-1, N-1));
}