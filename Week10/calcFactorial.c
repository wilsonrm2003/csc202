//programing example from Data Structures Using C by Reema Thareja
#include <stdio.h>

int Fact(int); // declare func
int main() {
    int num, val;
    printf("\nEnter number: ");
    scanf("%d", &num);
    val = Fact(num);
    printf("\n Factorial of %d = %d", num, val);
}

int Fact(int n){
    if (n == 1)
        return 1;
    else
        return (n * Fact(n-1));
}