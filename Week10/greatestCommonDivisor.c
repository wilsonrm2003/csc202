//programing example from Data Structures Using C by Reema Thareja
#include <stdio.h>

int GCD(int, int);
int main(){
    int num1, num2, res;
    printf("enter 2 numbers: ");
    scanf("%d %d", &num1, &num2);
    res = GCD(num1, num2);
    printf("\n GCD of %d and %d = %d", num1, num2, res);
    return 0;
}

int GCD(int num1, int num2){
    int rem;
    rem = num1 % num2;
    if (rem == 0)
        return num2;
    else 
        return (GCD(num2, rem));
}