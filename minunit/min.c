int min(int a[], int size) {
    int mini = a[0];
    
    for (int i = 1; i < size; i++) {
        if (a[i] < mini){
            mini = a[i];
        }
    }
    return mini;
}