// write a program that checks if (), [], {}. is a vaild expression
// output: Invalid/Valid
#include <stdio.h>
#include <string.h>

#define MAX 100

char st[MAX];
int top = -1;
void push(char st[], char);
char pop(char st[]);
void isvalid(char source[]);

int main() {
    char expression[MAX], target[MAX];
    printf("enter expression: ");
    scanf("%s", expression);
    strcpy(target, "");
    isvalid(expression);
    return 0;
}

void isvalid(char source[]) {

    printf("Valid");
}

void push(char st[], char val) {
    if (top == MAX-1)
        printf("\n STACK OVERFLOW");
    else {
        top++;
        st[top] = val;
    }
}

char pop(char st[]) {
    char val = ' ';
    if (top == -1) 
        printf("\n STACK UNDERFLOW");
    else {
        val = st[top];
        top--;
    }
    return val;
}