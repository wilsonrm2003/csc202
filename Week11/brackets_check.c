//programing example from Data Structures Using C by Reema Thareja
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX 10

int top = -1;
int stk[MAX];
void push(char);
char pop();

void main() {
    char exp[MAX], temp;
    int i, flag = 1;
    printf("enter expression: ");
    scanf("%s", exp);
    for (i = 0; i < strlen(exp); i++) {
        if (exp[i] == '(' || exp[i] == '{' || exp[i] == '[')
            push(exp[i]);
        if (exp[i] == ')' || exp[i] == '}' || exp[i] == ']'){
            if (top == -1) {
                flag = 0;
            }
            else {
                temp = pop();
                if (exp[i] == ')' && (temp == '{' || temp =='['))
                    flag = 0;
                if (exp[i] == '}' && (temp == '(' || temp =='['))
                    flag = 0;
                if (exp[i] == ']' && (temp == '(' || temp =='{'))
                    flag = 0;
            }
        }
    }
    if (top >= 0)
        flag = 0;
    if (flag == 1)
        printf("Valid\n ");
    else
        printf(" Invalid\n");   
}

void push (char c){
    if(top == (MAX-1))
        printf("Stack overflow");
    else {
        top = top + 1;
        stk[top] = c;
    }
}

char pop() {
    if (top == -1) {
        printf("Stack Underflow");
     }
    else {
        return(stk[top--]);
    }
}