#include <stdio.h>

/* 
    make a program that counts the number of non zero elements in a linked list
*/

struct node {
    int val;
    struct node* next;
}; // the linked list
typedef struct node nodes;

void numberOfNonZero(nodes *head) {
    nodes *temporary = head;
    int counter = 0;

    while (temporary != NULL){
        if (temporary->val != 0) {
            counter += 1;
        }
        temporary = temporary->next;

    }
    printf("%d \n", counter);
}



int main(){
    nodes n1, n2, n3, n4, n5, n6, n7;
    nodes *head;

    n1.val = 16;
    n2.val = 29;
    n3.val = 68;
    n4.val = 42;
    n5.val = 0;
    n6.val = 1;
    n7.val = 20;

    // linking the nodes
    head = &n4;
    n4.next = &n2;
    n2.next = &n3;
    n3.next = &n1;
    n1.next = &n7;
    n7.next = &n5;
    n5.next = &n6;
    n6.next = NULL;

    numberOfNonZero(head);

    return 0;
}