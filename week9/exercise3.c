#include <stdio.h>

/* 
    make a program that muliplies every element of the linked list with 10
*/
struct node {
    int val;
    struct node* next;
}; // the linked list
typedef struct node nodes;

void multiply_list(nodes *head) {
    nodes *temporary = head;

    while (temporary != NULL){
        printf("%d --> ", temporary->val * 10);
        temporary = temporary->next;

    }
    printf("\n");
}



int main(){
    nodes n1, n2, n3, n4;
    nodes *head;

    n1.val = 16;
    n2.val = 29;
    n3.val = 68;
    n4.val = 42;

    // linking the nodes
    head = &n4;
    n4.next = &n2;
    n2.next = &n3;
    n3.next = &n1;
    n1.next = NULL;

    multiply_list(head);

    return 0;
}