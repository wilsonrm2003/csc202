#include <stdio.h>

/* 
    make a program that merges two linked lists ;)
*/

struct node {
    int val;
    struct node* next;
};  
typedef struct node list_1, list_2;

struct node* MergeLists(struct node* a, struct node* b){
    struct node* result = NULL;

    if (a != NULL) {
        result = a;
        result->next = MergeLists(a->next, b);
    }
    if (a == NULL) {
        result = b;
    }
    if (b != NULL && a == NULL) {
        result->next = MergeLists(a, b->next);
    }
    return (result);
}

void print_list(struct node *head) {
    struct node *temporary = head;

    while (temporary != NULL){
        printf("%d --> ", temporary->val);
        temporary = temporary->next;

    }
    printf("\n");
}

int main(){
    // create values for first linked list
    list_1 n1, n2, n3, n4;
    list_1 *head_1; 

    n1.val = 16;
    n2.val = 29;
    n3.val = 68;
    n4.val = 42;
    // linking the nodes
    head_1 = &n4;
    n4.next = &n2;
    n2.next = &n3;
    n3.next = &n1;
    n1.next = NULL; // linked list 1 complete ;)

    // create values for second linked list
    list_2 m1, m2, m3, m4;
    list_2 *head_2;

    m1.val = 61;
    m2.val = 92;
    m3.val = 86;
    m4.val = 24;
    // linking the nodes
    head_2 = &m1;
    m1.next = &m3;
    m3.next = &m2;
    m2.next = &m4;
    m4.next = NULL; // linked list 2 complete ;)

    print_list(MergeLists(head_1, head_2));

    return 0;
}