#include <stdio.h>

struct node {
    int val;
    struct node* next;
}; 

typedef struct node list_1, list_2;

void print_list(struct node *head) {
    struct node *temporary = head;

    while (temporary != NULL){
        printf("%d --> ", temporary->val);
        temporary = temporary->next;

    }
    printf("\n");
}

struct node* SortedMerge(struct node* a, struct node* b){
    struct node* result = NULL;

    if (a == NULL)
        return (b);
    else if (b == NULL)
        return (a);

    if (a->val <= b->val){
        result = a;
        result->next = SortedMerge(a->next,b);
    }
    else {
        result = b;
        result->next = SortedMerge(a, b->next);
    }
    return (result);
    
}

int main(){
    // create values for first linked list
    list_1 n1, n2, n3, n4;
    list_1 *head_1; 

    n1.val = 16;
    n2.val = 29;
    n3.val = 42;
    n4.val = 68;
    // linking the nodes
    head_1 = &n1;
    n1.next = &n2;
    n2.next = &n3;
    n3.next = &n4;
    n4.next = NULL; // linked list 1 complete ;)

    // create values for second linked list
    list_2 m1, m2, m3, m4;
    list_2 *head_2;

    m1.val = 24;
    m2.val = 61;
    m3.val = 86;
    m4.val = 92;
    // linking the nodes
    head_2 = &m1;
    m1.next = &m2;
    m2.next = &m3;
    m3.next = &m4;
    m4.next = NULL; // linked list 2 complete ;)

    print_list(SortedMerge(head_1, head_2));

    return 0;
}