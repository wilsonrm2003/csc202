#include <stdio.h>

/* Write a program using pointers to interchange the second biggest
 and the second smallest number in the array*/

int main(){
    int i = 0;
    
    int small2location, big2location = 0;

    /*int size = 10;
    int a1[] = {5,2,6,1,6,8,20,6,9,11};*/
    int size = 10;
    int a1[] = {4,7,2,0,1,7,8,10,25,67};
    
    
    printf("\n array before [");
    for (i=0; i < 10; i++){
        printf(" %d ", *(a1+i));
    }
    printf("]");

    int small, big = *(a1);

    for(i=0; i < size; i++){ // calculates the smallest number in the array
        if (*(a1+i) < small){
            small = *(a1+i);
        }
    }

    for(i=0; i < size; i++) { // calculates the biggest number in the array
        if (*(a1+i) > big) {
            big = *(a1+i);
        }
    }

    int big2, small2 = *(a1);

    for(i=0; i < size; i++) { // calculates the second smallest number 
        if (*(a1+i) != small) {
            if (*(a1+i) < small2){
                small2 = *(a1+i);
                small2location = i;
            }
        }
    }


    for(i=0; i < size; i++) { // calculates the second biggest number 
        if (*(a1+i) != big) {
            if (*(a1+i) > big2){
                big2 = *(a1+i);
                big2location = i;
            }
        }
    }

    *(a1+small2location) = big2;
    *(a1+big2location) = small2;

    printf("\n array after [ ");
    for (i=0; i < 10; i++){
        printf(" %d ", *(a1+i));
    }
    printf(" ]");
    return 0;
 }