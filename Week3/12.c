#include <stdio.h>
/* Write a program to read two floating point number arrays. 
Merge the two arrays and display the resultant array in reverse order.*/

int main(){
    int i, index = 0;
    float array1[] = {8.4, 12.3, 8.9, 9.9, 8.7};
    float array2[] = {17.3, 18.7, 19.9, 39.9}; 
    float array3[9];
    
    for(i = 0; i < 5; i++){
        array3[index] = array1[i];
        index++;
    }
    for(i = 0; i<4; i++){
        array3[index] = array2[i];
        index++;
    }

    for (i = 0; i < 9; i++){
        printf("%f,", array3[i]);
    }
    return 0;
}