#include <stdio.h>
/* Write a program to read a floating point array.
 Update the array to insert a new number at the specified location.*/

int main(){
    int i, size, loc = 0;
    float num, arr[20];

    printf("How many elements do you want in your floating array: ");
    scanf("%d", &size);
    for(i=0; i < size; i++){
        printf("\n arr[%d] = ", i);
        scanf("%f", &arr[i]);
    }

    printf("\n Your array elements are: ");
    for(i=0; i< size; i++){
        printf(" %f ", arr[i]);
    }

    printf("\n Where would you like to add a number: ");
    scanf("%d", &loc);
    printf("\n What number do you want to add? ");
    scanf("%f", &num);

    for (i=loc; i<size+1; i++){
        arr[i+1] = arr[i];
    }
    
    arr[loc] = num;

    printf("\n Your new array elements are: ");
    for(i=0; i< size+1; i++){
        printf(" %f ", arr[i]);
    }

    return 0;
}