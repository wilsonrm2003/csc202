#include <stdio.h> 

/*Write a program to compute the sum and mean of
the elements of a two-dimensional array.*/

int main(){
    int twod[3][2] = {3, 5, 6, 7, 10, 17};
    int sum, mean = 0;
    //calculating sum and mean
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j< 2;j++) {
            sum = sum + twod[i][j];
        }
    mean = sum/6;
    }
    printf("the sum of the two-dimensional array is: %d", sum);
    printf("\nthe mean of the two dimensional array is: %d", mean);
}