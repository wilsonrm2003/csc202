#include <stdio.h>
/* Write a program that computes the product of the elements
 that are stored on the diagonal above the main diagonal*/

int main(){
    int product = 1;
    int arr[] = {5, 9, 4, 5, 8, 4, 1, 3, 2, 1, 7, 1, 2, 1, 2, 9}; //diagonal wise mapping [1,2,1,2,9]
    for (int i = 11; i < 16; i++){
    product = product*arr[i];
    }
    printf("product %d", product);
    return 0;
}