#include <stdio.h>

#define MAXLEN 50

int main(){
    typedef struct{
        char first_name[MAXLEN];
        char mid_name[MAXLEN];
        char last_name[MAXLEN]; 
    } NAME;
    typedef struct{
        char area[MAXLEN];
        char city[MAXLEN];
        char state[MAXLEN];
    } ADDRESS;
    struct employee{
        int emp_id;
        NAME name;
        ADDRESS address;
        int age;
        int salary;
        char designation[MAXLEN];
    };
    struct employee emply[10];
    for(int i = 0; i < 10; i++){
        scanf("%d", &emply[i].emp_id);
        scanf("%s %s %s", emply[i].name.first_name, emply[i].name.mid_name, emply[i].name.last_name);
        scanf("%s %s %s", emply[i].address.area, emply[i].address.city, emply[i].address.state);
        scanf("%d", &emply[i].age);
        scanf("%d", &emply[i].salary);
        scanf("%s", emply[i].designation);
    }
    for (int i = 0; i < 10; i++){
        printf("emp_id is: %d\n", emply[i].emp_id);
        printf("employee name is: %s %s %s\n", emply[i].name.first_name, emply[i].name.mid_name, emply[i].name.last_name);
        printf("employee addres is: %s %s %s\n", emply[i].address.area, emply[i].address.city, emply[i].address.state);
        printf("employee age is: %d\n", emply[i].age);
        printf("employee salary is: $%d\n", emply[i].salary);
        printf("employee position is: %s\n", emply[i].designation);
    }
}