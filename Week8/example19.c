#include <stdio.h>

int main(){
    struct time{
        int hr;
        int min;
        int sec;
    };
    struct time start_time, end_time;
    
    scanf("%d %d %d", &start_time.hr, &start_time.min, &start_time.sec);
    scanf("%d %d %d", &end_time.hr, &end_time.min, &end_time.sec);
    int leave = 1;

    while(leave != 0){
        printf("\n%dhr(s)      %dmin(s)     %dsec(s) have passed", start_time.hr,start_time.min,start_time.sec);
        
        start_time.sec += 1;

        if (start_time.sec >= 60) {
            start_time.sec = 0;
            start_time.min += 1;
        }
        
        if (start_time.min >= 60) {
            start_time.min = 0;
            start_time.hr += 1;
        }
        if (start_time.hr == end_time.hr){
            if (start_time.min == end_time.min){
                if (start_time.sec == end_time.sec){
                    leave = 0;
                }
            }
        }
    }
    printf("\nEnd time has been reached.    final time: %dhr(s) %dmin(s) %dsec(s)", start_time.hr, start_time.min, start_time.sec);
}