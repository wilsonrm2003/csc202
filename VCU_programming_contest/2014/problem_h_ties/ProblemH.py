import math
import numbers

problems = input()
problems = int(problems)
while problems > 0:
    num_elements = input()
    num_elements = int(num_elements)
    elements = input()
    elements = [int(n) for n in elements.split()]
    while num_elements != 2:
        new = []
        for x in range(math.ceil(num_elements/2)):
            new.append(elements[x]+elements[num_elements-x-1])
        elements = new
        num_elements = math.ceil(num_elements/2)
    if elements[0] > elements[1]:
        print("Ana")
    else:
        print("Voyo")
    problems -= 1
