problems = input()
problems = int(problems)
while problems > 0:
    test_case = input()
    test_case = [int(n) for n in test_case.split()]
    sum_ = test_case[0]
    difference = test_case[1]
    if sum_ >= difference:
        score2 = (difference - sum_) / -2
        score1 = sum_ - score2
        if score1 > score2:
            print(f"{score1} {score2}")
        else:
            print(f"{score2} {score1}")
    else:
        print("impossible")
    problems -= 1