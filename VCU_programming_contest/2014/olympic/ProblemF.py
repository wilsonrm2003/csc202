import math 
cases = input()
cases = int(cases)
while cases != 0:
    data = input()
    data = [int(n) for n in data.split()]
    num_contestents = data[0]
    data.remove(num_contestents)
    scores = data
    average = sum(scores) / num_contestents
    #percentage
    count = 0 
    for score in scores:
        if score > average:
            count += 1
    above_avg = (count / num_contestents) * 100
    above_avg = int(round(above_avg, 0))
    print(above_avg )
    cases -= 1