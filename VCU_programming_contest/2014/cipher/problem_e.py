alphabet = "abcdefghijklmnopqrstuvwxyz"

#gather data
pad_size = input()
pad_size = int(pad_size)
pad = input()
pad = [int(n) for n in pad.split()]
stop = 1 

while 1 == 1:
    plain_text = input()
    cipher_text = ""

    if plain_text == "-1":
        break
    count = 0
    for letter in plain_text:
        loc = alphabet.find(letter)
        if loc == -1:
            cipher_text += " "
        else:
            cipher_text += alphabet[(loc + pad[count]) % 26]
            count = (count + 1) % pad_size
    print(cipher_text)