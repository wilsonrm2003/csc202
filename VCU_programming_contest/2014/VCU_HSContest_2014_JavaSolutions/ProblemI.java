import java.util.*;
public class ProblemI
{
	public static void main(String args[]) throws Exception
	{
		Scanner cin = new Scanner(System.in);
		int n,sum,dif;
		n = cin.nextInt();
		while(n>0)
		{
			sum = cin.nextInt();
			dif = cin.nextInt();
			if(dif>=0 && sum>=dif && (dif+sum)%2==0)
			{
				System.out.printf("%d %d\n",(dif+sum)/2,(sum-dif)/2);
			}
			else
				System.out.printf("impossible\n");
			n--;
		}
	}
}