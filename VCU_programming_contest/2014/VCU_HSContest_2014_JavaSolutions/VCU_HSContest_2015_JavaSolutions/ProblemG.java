import java.util.Scanner;

public class ProblemG{
 
 public static void main(String[] args){
  Scanner in = new Scanner(System.in);
  long n = 0;
  long w = 0;
  long d = 0;
  long r = 0;

        while(in.hasNextLine()){
  Scanner lineScanner = new Scanner(in.nextLine());
  int num_farms = lineScanner.nextInt();

  while(num_farms>0){
   lineScanner = new Scanner(in.nextLine());
   n = lineScanner.nextLong();
   w = lineScanner.nextLong();
   d = lineScanner.nextLong();
   r = lineScanner.nextLong();

   System.out.println(milk_cows(n, w, d, r) + " m");
            num_farms--;
  }
    }

 }
 public static long milk_cows(long n, long w, long d, long r){

        long L2=0;
        for (long i=1; i<=n; i++){
            L2 = L2 + 2*(i-1)*r;
        }
        long L = 2*n*(w+d) + L2;
        
  //long big_l1 = 2L*w + 2L*d;
  //long ln = 2L*(n-1)*r;
        //System.out.println(n*big_l1 + n*(ln)/2L);
        
        return L;
 }
}