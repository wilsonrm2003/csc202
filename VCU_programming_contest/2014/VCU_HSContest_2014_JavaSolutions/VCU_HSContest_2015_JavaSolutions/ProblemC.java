/**
 * 
 * Encryption at its Finest
 * 
 */

//package encryption;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProblemC {
	
	/**
	 * Encrypt the given plaintext
	 * @param plaintext
	 * @param key
	 * @param maxcharcount
	 * @return the ciphertext 
	 */
	public static String encrypt(String plaintext, int [] key, int maxcharcount)
	{
		if(key.length<maxcharcount)
			maxcharcount=key.length;
		StringBuilder cyphertext=new StringBuilder(maxcharcount);
		int charindex=0;
		for (char ch : plaintext.toCharArray())
		{	
			if(!Character.isLetter(ch))			
				cyphertext.append(ch);
			else
			{
				if(charindex==maxcharcount)
					break;
				if(Character.isUpperCase(ch))
					ch=Character.toLowerCase(ch);
				char newchar=(char)('a'+(ch-'a'+key[charindex])%26);
				cyphertext.append(newchar);
				charindex++;
			}
		}
		return cyphertext.toString();
	}
	
	

	/**
	 * Main function
	 * @param args
	 */
	public static void main(String args[])
	{ 
			encryptTest();

	}

	/**
	 * Function to test encryption
	 */
	public static void encryptTest()
    {
        //System.out.println("Input an integer n to specify the number of secret keys.");
		BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
		int length=getLength(reader);
        //System.out.println("Input n number of intergers to specify the secret keys.");
		int [] key=getKey(reader);
        //System.out.println("Input n number of small letters to specify the plaintext.");
        //System.out.println("Input -1 as the end of input.");
        //System.out.println("The program will return the corresponding ciphertext.");

        String plaintext=getText(reader);
  
		String cipherText=encrypt(plaintext, key,length);
		System.out.println(cipherText);
	}
	
	

	/**
	 * Read the length of the key from stdin
	 * @return length
	 */
	private static int getLength(BufferedReader reader)
	{
		
		while(true)
		{
			try
			{
				int length=Integer.parseInt(reader.readLine());
				return length;
			}
			catch(Exception e)
			{
				System.out.println("The number of secret keys should be an integer!");
			}
		}
	}
	
	/**
	 * Read the key from stdin
	 * @return key
	 */
	private static int [] getKey(BufferedReader reader)
	{
		while(true)
		{
			try
			{
				String keylist=reader.readLine();
				String [] keys=keylist.split(" ");
				int [] key=new int[keys.length];
				for (int i=0;i<keys.length;i++)
					key[i]=Integer.parseInt(keys[i]);
				return key;
			}
			catch(Exception e)
			{
				System.out.println("Secret keys should be n number of integers!");
			}
			
		}
	}
	
	/**
	 * Read the plain(cipher) text from stdin
	 * @return plain(cipher) text
	 */
	private static String getText(BufferedReader reader)
	{

		String ciphertext="";
		while(true)
		{
			try
			{
 				String curline=reader.readLine();
				if(curline.equals("-1"))
					return ciphertext;
				ciphertext+=curline+"\n";
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}

