import java.util.Scanner;
import java.util.LinkedList;
import java.util.Queue;
import java.util.HashSet;

/**
 @author Shaun
**/
public class ProblemJ{
	public Queue<Node> q;
	public HashSet<String> dict;
	public static void main(String[] args){
		ProblemJ pj = new ProblemJ();

		Scanner in = new Scanner(System.in);
		Scanner lineScanner = new Scanner(in.nextLine());

		int num_instances = lineScanner.nextInt();
		for(int ii=0; ii<num_instances; ii++){
			pj.q = new LinkedList<Node>();
			pj.dict = new HashSet<String>();
			lineScanner = new Scanner(in.nextLine());
			String start_word = lineScanner.next();
			String end_word = lineScanner.next();
			int dict_size = lineScanner.nextInt();
			
			//create a dictionary
			pj.dict.add(end_word);
			for(int i=0; i<dict_size; i++){
				lineScanner = new Scanner(in.nextLine());
				String temp_word = lineScanner.next();
				pj.dict.add(temp_word);
			}
			pj.q.add(new Node(null, start_word, 0));

			//build tree-graph breadth-wise
			//when end_word is encountered, stop
			while(!pj.q.isEmpty()){
				Node temp = pj.q.remove();
				if(temp.value.equalsIgnoreCase(end_word)){
					System.out.println((temp.height_pos-1));
					break;
				}
				pj.queueChildren(temp);
			}
		}
	}


	private void queueChildren(Node parent){
		dict_loop: for(String s : dict){
			Node temp_ref = parent;
			while(temp_ref.parent != null){
				//System.out.println("temp_ref.parent val " + temp_ref.parent.value);
				if(s.equalsIgnoreCase(temp_ref.parent.value)){
					continue dict_loop;
				}
				temp_ref = temp_ref.parent;
			}
			if(offByOne(parent.value, s)){
				//System.out.println("adding " + s + " to queue, " + parent.value + " was off by one");
				q.add(new Node(parent, s, parent.height_pos+1));
			}
		}
	}
	private static boolean offByOne(String a, String b){
		int distance = 0;
		for(int i=0; i<a.length(); i++){
			if(!(a.charAt(i) == b.charAt(i))){
				distance++;
			}
		}
		if(distance == 1){
			return true;
		}
		return false;
	}
	
}
class Node{
	public Node parent = null;
	public String value; //string value this node stores
	public int height_pos;//level of this node in tree-like graph
	public Node(Node p, String s, int h){
		parent = p;
		value = s;
		height_pos = h;
	}
}
