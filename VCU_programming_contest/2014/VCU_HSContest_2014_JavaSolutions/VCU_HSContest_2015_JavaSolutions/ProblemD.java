import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.lang.Math;

public class ProblemD {
    
    private Vector<String>  Overlaps = new Vector<String> ();


    private Vector<Integer> Position0 = new Vector<Integer> ();
    private Vector<Integer> Position1 = new Vector<Integer> ();
    
    private Hashtable<String, Vector<String>> Pairs = new Hashtable<String, Vector<String>>();
    
    private Hashtable<Integer, Integer> Results = new Hashtable<Integer, Integer>(); 
    
    public ProblemD(String dolphin0, String dolphin1) { 

	//  get the individual words from the string
	String[] words0 = dolphin0.substring(0, dolphin0.length()).split("\\s+");
	String[] words1 = dolphin1.substring(0, dolphin1.length()).split("\\s+");
	
	//  get the overlap in words
	for(Integer i = 0; i < words0.length; i++) {	
	    for(Integer j = 0; j < words1.length; j++) {	
		if(words0[i].equals(words1[j])) {  
		    Overlaps.add(words0[i]); 
		    Position0.add(i); 
		    Position1.add(j); 
		}
	    }
	}
	
	//  put the overlaps into pairwise relations based on order
	for(int i = 0; i < Overlaps.size(); i++) {	
	    for(int j = 0; j < Overlaps.size(); j++) {	
		
		if(!Overlaps.get(i).equals(Overlaps.get(j))) { 
		    if( (Position0.get(i) < Position0.get(j)) && 
			(Position1.get(i) < Position1.get(j)) ) { 
			
			Vector<String> temp = new Vector<String> ();
			if(Pairs.containsKey(Overlaps.get(i))) { 
			    temp = Pairs.get(Overlaps.get(i)); 
			    if(! (temp.contains(Overlaps.get(j)))) { 
				temp.add(Overlaps.get(j));
			    Pairs.put(Overlaps.get(i), temp); 
			    }
			}
			else { 
			    temp.add(Overlaps.get(j)); 
			    Pairs.put(Overlaps.get(i), temp); 
			}
		    }
		}
	    }
	}

	// get the variable length sequences using a DFS 
	// storing the results length and frequency in the
	// Results vector
	for(int i = 0; i < Overlaps.size(); i++) {	
	    String path = Overlaps.get(i); 
	    Integer length = 1; 	    
	    DFS(Overlaps.get(i), path, length); 
	}

	//  Loop through the Results Vector and print out the lengths greater than zero
       	Set set = Results.keySet();
	Iterator itr = set.iterator();
	while(itr.hasNext()) { 
	    Integer l = (Integer) itr.next();
	    Integer f = Results.get(l);
	    System.out.println(l + " " + f);
        }
    }
    
    
    //  Standard DFS but it is stroing the length and frequency count
    private void DFS(String wrd, String path, Integer length) { 
	
	Integer freq = 0; 
	
	if(Results.containsKey(length)) { 
	    freq = Results.get(length); 
	}
	
	freq++; 

	       Results.put(length, freq); 

	       //System.err.println(wrd + " : " + path + " : " + length + " : " + freq); 
	
	Vector<String> Children = new Vector<String>(); 
	
	Children = getChildren(wrd); 
 
	for(int i = 0; i < Children.size(); i++) {	
	    
	    String npath = path + " " + Children.get(i); 
	    Integer nlength = length + 1; 
	    DFS(Children.get(i), npath, nlength); 
	}
    }


    // Standard getChildren function
    private Vector<String> getChildren(String wrd) { 
	
	Vector<String> Children = new Vector<String>(); 

	if(Pairs.containsKey(wrd)) { 
	    Children = Pairs.get(wrd); 
	}
	
	return Children; 
    }

    
    public static void main(String[] args) throws IOException {
	

	BufferedReader br = 
	    new BufferedReader(new InputStreamReader(System.in));
 
	String dolphin0;
	String dolphin1;
	
	dolphin0 = br.readLine(); 
	dolphin1 = br.readLine(); 
	
	ProblemD d = new ProblemD(dolphin0, dolphin1); 
    }
    
} 
