import java.io.*;
import java.util.*;


/* 
The input graph is a tree. 

The pessimistic scenario involves orienting edges in a way that there are no paths of lengths >1. 
Then, we only have N-1 paths of length 1. That's the lowest possible number of paths. 

The highest possible number of paths can be obtained in this way:
A vertex V with m neighbors can be seen as a root of m subtrees, each containing one of the m neighbors of V.
Within a single subtree, the highest number of paths can be obtained is all edges are directed towards the root, or away from the root - and these two options give the same number of paths. 
If all subtrees are directed into V, or all are out of V, no paths involve nodes from >1 subtree. 
However, is some subtrees are oriented "into V" and some "away from V", there are additional paths between all nodes in "into" subtrees and all nodes in "away" subtrees.
The best combination of "into" vs "away" for a vertex V can be obtain using a 0/1 knapsack problem

We repeat the process for every vertex V, and pick the best one (a supervertex). 

An interesting simple example is a graph:
a-b
a-c
a-d
a-e

with solution 
b->a
c->a
a->d
a->e

See also Section 2 of http://arxiv.org/pdf/cs/0205042.pdf
*/

public class ProblemB
{
	
  public static int solve(int adjMx[][],int N)
  {
	  int subtreePathCnt[][]=new int[N][N];
	  int subtreeNodeCnt[][]=new int[N][N];
	  cntSubtreeAll(adjMx,N,subtreePathCnt,subtreeNodeCnt);
	  return findSupervertexAndScoreIt(adjMx,N,subtreePathCnt,subtreeNodeCnt);
  }
  
  public static void cntSubtreeAll(int adjMx[][],int N, int subtreePathCnt[][],int subtreeNodeCnt[][])
  {
	  int res[];
	  for (int i=0;i<N;i++)
		for (int j=0;j<N;j++)
		{
			if (adjMx[i][j]==1)
			{
				// count nodes & paths in subtree rooted at i that contains vertex j
				res=cntSubtree(adjMx,N,i,j);
				subtreePathCnt[i][j]=res[0];
				subtreeNodeCnt[i][j]=res[1];
				subtreePathCnt[i][j]+=subtreeNodeCnt[i][j];
			}
			else
			{
				subtreePathCnt[i][j]=0;
				subtreeNodeCnt[i][j]=0;
			}
		}
  }
    
  public static int[] cntSubtree(int adjMx[][],int N, int eSrc,int eTgt)
  {
	  int pathCnt=0;
	  int nodeCnt=0;
	  int res[];
	  for (int i=0;i<N;i++)
		if ( (adjMx[eTgt][i]==1) && (eTgt!=i) && (eSrc!=i) )
		{
			res=cntSubtree(adjMx,N,eTgt,i);
			pathCnt+=res[0];
			nodeCnt+=res[1];
		}
	pathCnt+=nodeCnt;
	nodeCnt+=1;
	return new int[] {pathCnt, nodeCnt};
  }
  
  public static int findSupervertexAndScoreIt(int adjMx[][], int N, int subtreePathCnt[][],int subtreeNodeCnt[][])
  {
	  int score[]=new int[N];
	  int inScore[]=new int[N];
	  int outScore[]=new int[N];
	  int values[]=new int[N];
	  int orientations[]=new int[N];
	  int innerPathsCnt,subtreeCnt;
	  int maxScore=-1;
	  for (int i=0;i<N;i++)
	  {
		  innerPathsCnt=0;
		  subtreeCnt=0;
		  for (int j=0;j<N;j++)
		  {
			  innerPathsCnt+=subtreePathCnt[i][j];
			  if (subtreeNodeCnt[i][j]>0)
			  {
				  values[subtreeCnt]=subtreeNodeCnt[i][j];
				  subtreeCnt++;				  
			  }
		  }
		  // pick the orientation of trees going in vs trees going out
		  // maximize (nodesInTreesGoingIn * nodesInTreesGoingOut)
		  // total #nodes is the trees is N-1 nodes, so max achievable in theory is ((N-1)/2)^2, 
		  // and we want to find subset as close to (N-1)/2 as possible
		  knapsack01((int)Math.floor((N-1.0)/2.0),subtreeCnt,values,values,orientations);
		  inScore[i]=0;
		  outScore[i]=0;
		  for (int j=0;j<subtreeCnt;j++)
		  {
			  if (orientations[j]==1)
			  {
				  outScore[i]+=values[j];
			  }
			  else
			  {
				  inScore[i]+=values[j];
			  }
		  }
		  // total paths = nodesInTreesGoingIn * nodesInTreesGoingOut + paths inside trees
		  score[i]=inScore[i]*outScore[i]+innerPathsCnt;
		  if (score[i]>maxScore)
			maxScore=score[i];
	  }	  
	  return maxScore;
  }

	
  public static int knapsack01(int W, int n, int weights[], int values[], int selection[])
  {
	  int V[][]=new int[n+1][W+1];
	  int keep[][]=new int[n+1][W+1];
  	  for (int i=0;i<n;i++)
		selection[i]=0;
	  for (int i=0;i<=n;i++)
	  {
		  V[i][0]=0;
		  keep[i][0]=0;
	  }
	  for (int i=0;i<=W;i++)
	  {
		  V[0][i]=0;
		  keep[0][i]=0;
	  }
	  for (int i=1;i<=n;i++)
	  {
		  for (int w=0;w<=W;w++)
		  {
			  if ( (weights[i-1]<=w) && ( V[i-1][w] < values[i-1]+V[i-1][w-weights[i-1]] ) )
			  {
				  V[i][w]=values[i-1]+V[i-1][w-weights[i-1]];
				  keep[i][w]=1;
			  }
			  else
			  {
				  V[i][w]=V[i-1][w];
				  keep[i][w]=0;
			  }
		  }
	  }
	  int kScore=V[n][W];
	  int K=W;
	  for (int i=n;i>0;i--) 
		if (K>=0 && keep[i][K]==1)
		{
			selection[i-1]=1;
			K=K-weights[i-1];
		}
	  return kScore;
	}



  public static void main(String[] args) throws Exception
  {
    Scanner sc=new Scanner(System.in);
    int N=sc.nextInt();
    int [][] adjMx=new int[N][N]; // should in fact be implemented using adjacency lists instead of adjacency matrix; that's left as an exercise to readers
    int v1,v2;
    for (int i=0;i<N-1;i++)
    {
		v1=sc.nextInt()-1;
		v2=sc.nextInt()-1;
		adjMx[v1][v2]=1;
		adjMx[v2][v1]=1;
	}
	int lo=N-1;
	int hi=solve(adjMx,N);
    System.out.println(lo+" "+hi);
  }

}

