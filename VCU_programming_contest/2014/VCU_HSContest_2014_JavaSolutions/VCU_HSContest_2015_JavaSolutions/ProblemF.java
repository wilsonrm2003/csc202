
import java.io.*;
import java.util.*;



public class ProblemF {
	
	public static void main(String[] args) throws IOException {
	    Scanner scanner = new Scanner( System.in );
	    while (true) {	
		    int m = scanner.nextInt(),  // Number of students
				n = scanner.nextInt();	// Number of dishes		    
		    if (m == 0 && n == 0)
		    	break;
			Directed_graph g = new Directed_graph(2*n+1), gr = new Directed_graph(2*n+1);				
			
		    for(int i=0; i < m; i++) {
		    	int wi = scanner.nextInt(),
					vi = scanner.nextInt();			
				// Create the graph by mapping literal -n..-1 and 1..n
				// to 0..(n-1) and n+1,..,2n
		    	g.add_edge(-wi +n, vi +n);
				g.add_edge(-vi +n, wi +n);
				// Construct also the reverse graph
				gr.add_edge(vi +n, -wi +n);
				gr.add_edge(wi +n, -vi +n);
		    }		    
			// Find Strongly connected components using Kosaraju's algorithm
			// http://en.wikipedia.org/wiki/Kosaraju's_algorithm		
			ArrayList<Integer>	S = new ArrayList<Integer>();
			boolean visited[] = new boolean[2*n+1]; // all defaults to false
			for(int u = 0; u < 2*n+1; ++u) 
				if (!visited[u]) 
					dfs(u, g, visited, S);
			Collections.reverse(S);
			Arrays.fill(visited, false);
			int[]	marks = new int[2*n+1];
			boolean answer= true;
			for(Integer u: S) 
				if (answer && !visited[u]) {
					ArrayList<Integer> Scc = new ArrayList<Integer>();
					dfs(u, gr, visited, Scc);
					for(Integer v: Scc) {
						if (v > n)
								v = 2*n-v;
						if (marks[v] == u+1) {  // Both u and ~u in the same Scc
							answer= false;
							break;
						}					
						marks[v] = u+1;
					}
				}
			if (answer)
				System.out.println("YES");
			else
				System.out.println("NO");		
	    }
	    scanner.close();
	    return;
	}

	private static void dfs(int u, Directed_graph g, boolean[] visited,
			ArrayList<Integer> vorder) {
		visited[u] = true;
		for(Integer v: g.neighbors(u)) 
			if (!visited[v]) 
				dfs(v, g, visited, vorder);			
		vorder.add(u);
	}
}

class	Directed_graph {	
	private		ArrayList< ArrayList<Integer> >  g;
	
	public		Directed_graph(int num_node) {	
		g = new ArrayList< ArrayList<Integer> >(num_node);
		for(int i=0;i<num_node;++i)
			g.add( new ArrayList<Integer>() );
	}
	
	
	public ArrayList<Integer> neighbors(int u){
		return g.get(u);
	}
	
	public	void	add_edge(int u, int v) {
		neighbors(u).add(v);		
	}
}

