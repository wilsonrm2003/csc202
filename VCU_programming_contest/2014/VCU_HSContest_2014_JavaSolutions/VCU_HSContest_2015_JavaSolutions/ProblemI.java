
import java.util.Scanner;
import java.math.BigInteger;

/**
 * Created by jacksonds88 on 2/19/15.
 */
public class ProblemI
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        while(scan.hasNextInt())
        {
            int steps = scan.nextInt();
            BigInteger count = solution(steps);
            System.out.println(count);
        }
    }

    static BigInteger solution(int n)
    {
        n++;
        if(n == 0 || n == 1)
            return new BigInteger(n + "");

        BigInteger[] F = new BigInteger[n];
        // F[-1] = 0; // starting reference
        F[0] = new BigInteger(1 + "");
        F[1] = new BigInteger(1 + "");
        for(int i = 2; i < F.length; i++)
            F[i] = F[i-2].add(F[i-1]);

        return F[F.length-1];
    }
}
