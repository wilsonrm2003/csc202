import java.io.*;
import java.util.*;

/*
Greedy approach: sort events by their end times, 
and start by picking the first event, 
then first that doesn't conflict with the previous etc. 
See: Cormen et al. Chapter 16.1 "An activity-selection problem" 
*/



public class ProblemH
{


  public static int solve(Event events[],int N)
  {
   Arrays.sort(events);
   int cnt=1;
   int last=events[0].getEnd();
   for (int i=2;i<N;i++)
   {
    if (events[i].getStart()>last)
    {
     last=events[i].getEnd();
     cnt++;
    }   
   }
   return cnt;  
  }
  
  public static void main(String[] args) throws Exception
  {
    Scanner sc=new Scanner(System.in);
    int N=sc.nextInt();
    Event[] events=new Event[N];
    int v1,v2;
    for (int i=0;i<N;i++)
    {
  v1=sc.nextInt();
  v2=sc.nextInt();
  events[i]=new Event(v1,v2);
 }
    System.out.println(solve(events,N));
  }

}

class Event implements Comparable<Event>{
   private int start;
   private int end;

   Event(){};

   Event(int s, int e){
      start = s;
      end = e;
   }
   
   int getStart()
   {
    return start;
   }
   
   int getEnd()
   {
    return end;
   }

   // Overriding the compareTo method
   public int compareTo(Event d){
      return this.end-d.getEnd();
   }

}