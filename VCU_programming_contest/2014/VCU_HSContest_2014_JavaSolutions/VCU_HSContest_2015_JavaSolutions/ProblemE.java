import java.util.Scanner;

public class ProblemE {

        public static void main(String[] args){
            Scanner in = new Scanner(System.in);
            int n = 0;
            float r1 = 0;
            float r2 = 0;
            float r3 = 0;
            float r4 = 0;
            float r5 = 0;
            float fee = 0;
            float bestVal = -99;
            int bestIndex = -99;
            float val = -99;

            Scanner lineScanner = new Scanner(in.nextLine());
            n = lineScanner.nextInt();

            for (int i=1 ; i<=n ; i++) {
                lineScanner = new Scanner(in.nextLine());
                r1 = lineScanner.nextFloat();
                r2 = lineScanner.nextFloat();
                r3 = lineScanner.nextFloat();
                r4 = lineScanner.nextFloat();
                r5 = lineScanner.nextFloat();

                lineScanner = new Scanner(in.nextLine());
                fee = lineScanner.nextFloat();

                val = compute_funds(r1, r2, r3, r4, r5,fee);
                System.out.print(String.format("%.1f", val) + " ");

                if ( val > bestVal ) {
                    bestVal = val;
                    bestIndex = i;
                }
            }

            System.out.println("");
            System.out.println(bestIndex);

        }
        public static float compute_funds(float a1, float a2, float a3, float a4, float a5, float b){
            float result = 0;
            float[] list = new float[5];
            list[0]=a1;
            list[1]=a2;
            list[2]=a3;
            list[3]=a4;
            list[4]=a5;

            for (int i=0; i<=4; i++) {
                result = (result + 100) * (1 + list[i] / 100);
                result = result * (1 - b / 100);
            }

            return (result/500-1)*100;
        }
}
