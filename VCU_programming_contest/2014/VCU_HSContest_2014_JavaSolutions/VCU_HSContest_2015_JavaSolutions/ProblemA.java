import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by jacksonds88 on 2/18/15.
 */
public class ProblemA
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        while(scan.hasNextInt())
        {
            int n = scan.nextInt();
            int[] A = new int[n];
            for(int i = 0; i < A.length; i++)
                A[i] = scan.nextInt();
            //System.out.println(singleNumber(A));
            System.out.println(singleNumber2(A));
        }
    }

    // solution with XOR
    public static int singleNumber(int[] A)
    {
        int result = 0;
        for(int i : A) result ^= i;
        return result;
    }

    // solution with HashMap
    public static int singleNumber2(int[] A)
    {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(A.length/2 + 1); // set capacity

        for(int i = 0; i < A.length; i++)
            if(!map.containsKey(A[i]))
                map.put(A[i], 1);
            else
                map.remove(A[i]);

        if(map.size() == 1)
            return map.entrySet().iterator().next().getKey();
        else
            return 0; // if there is not a unique number, just return 0
    }
}
