
import java.io.*;
import java.util.Scanner;

public class ProblemG {
	final static int MAX_N = 501000;
	
	/**
	 * Find all primes at most max using the Sieve of Eratosthenes.
	 * @param max
	 * @return An array contains all prime between 2 and max
	 */
	static int[] prime(int max) {
        // initially assume all integers are prime
        boolean[] isPrime = new boolean[max + 1];
        for (int i = 2; i <= max; i++) 
            isPrime[i] = true;        

        // mark non-primes <= max using Sieve of Eratosthenes        
        for (int i = 2; i*i <= max; ++i) {
            // if i is prime, then mark multiples of i as nonprime
            // suffices to consider mutiples i, i+1, ..., N/i
            if (isPrime[i])             	
                for (int j = i+i; j <= max; j+=i) 
                    isPrime[j] = false;
            
        }
        // count the number of prime numbers
        int nPrime = 0;
        for (int i=2; i <= max; ++i)
        	if (isPrime[i])
        		++nPrime;
        // Put prime number into an array p
        int[] p = new int[nPrime];        
        for (int i=max; i >= 2; --i)
        	if (isPrime[i]) {
        		--nPrime;
        		p[nPrime] = i;
        	}        
        return p;
	}
	
	/**
	 * Return the value of F(N) = G(4) + ... + G(2N)
	 * @param N 
	 * @param p The precomputed array of prime numbers
	 * @return F(N)
	 */
	private static int	F(int N, int[] p) {
		int left = 1, right = 1;
		while (p[right] + p[left] <= N) 
			++right;
		int f = 1; // G(4)
		do {
		  while ((right >0) &&(p[right] + p[left] > N) )
			  --right;
		  f += right - left + 1;
		  ++left; 
		} while (left <= right);		
		return f;
	}
	
	public static void main(String[] args) throws IOException {
	    Scanner scanner = new Scanner( System.in );
	    
	    int[] p = prime(2*MAX_N); // Precompute the prime array
	    
	    int k = scanner.nextInt();
	    for(int i=0; i < k; i++) {
	    	int N = scanner.nextInt();
	    	System.out.println( F(2*N, p) );
	    }	    
	    scanner.close();
	}

}
