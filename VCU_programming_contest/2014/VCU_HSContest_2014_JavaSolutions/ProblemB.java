	import java.util.InputMismatchException;
	import java.util.Scanner;

	public class ProblemB{
		private boolean unsettled[];
		private boolean settled[];
		private int numberofvertices;
		private int adjacencyMatrix[][];
		private int key[];

		public static final int INFINITE = 99999;

		private int parent[];

		public ProblemB(int numberofvertices){
			this.numberofvertices = numberofvertices;
			unsettled = new boolean[numberofvertices];
			settled = new boolean[numberofvertices];
			adjacencyMatrix = new int[numberofvertices][numberofvertices];
			key = new int[numberofvertices];
			parent = new int[numberofvertices];
		}

		public int getUnsettledCount(boolean unsettled[]){
			int count = 0;
			for (int index = 0; index < unsettled.length; index++){
				if (unsettled[index]){
					count++;
				}
			}
			return count;
		}

		public void primsAlgorithm(int adjacencyMatrix[][]){
			int evaluationVertex;

			for (int source = 0; source < numberofvertices; source++){
				for (int destination = 0; destination < numberofvertices; destination++){
					this.adjacencyMatrix[source][destination] = adjacencyMatrix[source][destination];
				}
			}
			for (int index = 0; index < numberofvertices; index++){
				key[index] = INFINITE;
			}

			key[0] = 0;

			unsettled[0] = true;

			parent[0] = 0;

			while (getUnsettledCount(unsettled) != 0){
				evaluationVertex = getMimumKeyVertexFromUnsettled(unsettled);
				unsettled[evaluationVertex] = false;
				settled[evaluationVertex] = true;
				evaluateNeighbours(evaluationVertex);
			}
		} 

		private int getMimumKeyVertexFromUnsettled(boolean[] unsettled2){
			int min = Integer.MAX_VALUE;
			int node = 0;
			for (int vertex = 0; vertex < numberofvertices; vertex++){
				if (unsettled[vertex] == true && key[vertex] < min){
					node = vertex;
					min = key[vertex];
				}
			}
			return node;
		}

		public void evaluateNeighbours(int evaluationVertex){
			for (int destinationvertex = 0; destinationvertex < numberofvertices; destinationvertex++){
				if (settled[destinationvertex] == false){
					if (adjacencyMatrix[evaluationVertex][destinationvertex] != INFINITE){
						if (adjacencyMatrix[evaluationVertex][destinationvertex] < key[destinationvertex]){
							key[destinationvertex] = adjacencyMatrix[evaluationVertex][destinationvertex];
							parent[destinationvertex] = evaluationVertex;
						}
						unsettled[destinationvertex] = true;
					}
				}
			}
		}

		public void printCost(){
			int cost = 0;
			for (int vertex = 1; vertex < numberofvertices; vertex++){
				cost += adjacencyMatrix[parent[vertex]][vertex];
			}
			System.out.println(cost);
		}
		public static void main(String[] args){
			int adjacency_matrix[][];
			int number_of_vertices;

			Scanner inputscanner = new Scanner(System.in);
			Scanner linescanner = new Scanner(inputscanner.nextLine());
			int numVertices = linescanner.nextInt();
			int numEdges = linescanner.nextInt();
			int[][] costmatrix = new int[numVertices][numVertices];
			while(inputscanner.hasNextLine()){
				linescanner = new Scanner(inputscanner.nextLine());
				int firstvert = linescanner.nextInt();
				int secondvert = linescanner.nextInt();
				int weight = linescanner.nextInt();
				costmatrix[firstvert][secondvert] = weight;
				costmatrix[secondvert][firstvert] = weight;
			}
			for (int i = 0; i < numVertices; i++){
				for (int j = 0; j < numVertices; j++){
					if (costmatrix[i][j] == 0){
						costmatrix[i][j] = INFINITE;
					}
				}
			}
			ProblemB prims = new ProblemB(numVertices);
			prims.primsAlgorithm(costmatrix);
			prims.printCost();
			linescanner.close();
			inputscanner.close();
		}
	}