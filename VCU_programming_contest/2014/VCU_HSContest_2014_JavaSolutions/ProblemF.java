import java.util.Scanner;

public class ProblemF {

public static void main(String[] args) 
{
   Scanner s = new Scanner(System.in);
   int n = s.nextInt();
   for (int l=1;l<=n;l++)
   {
     int m=s.nextInt();
     int arr[]=new int[m];
     int sum=0;
     for(int i=0; i<m; i++)
     {
       arr[i]=s.nextInt();
       sum+=arr[i];
     }
     double mean=(double)sum/(double)m;
     int cnt=0;
     for(int i=0;i<m;i++)
     {
		if (arr[i]>mean)
			cnt++;
	 }
     int fraction=(int)Math.round(100.0*(double)cnt/(double)m);
     System.out.println(fraction);
   }
}
}
