import java.io.*;
import java.util.*;

public class ProblemJ
{
  public static int hmm_viterbi(int transitions[][], int initial[], int emissions[][], int observations[])
  {

    int INF=1000000000;
    int T=observations.length;
    int N=initial.length;
    int delta[][]=new int[T][N];
    int psi[][]=new int[T][N];
    int bestState[]=new int[T];

    int curr;

    for (int i=0;i<N;i++)
    {
      delta[0][i]=initial[i]+emissions[i][observations[0]];
      //System.out.println("t:0"+" i:"+i+" delta:"+delta[0][i]);
      psi[0][i]=0;
    }

    for (int t=1;t<T;t++)
    {
      for (int j=0;j<N;j++)
      {
        delta[t][j]=INF;
        for (int i=0;i<N;i++)
        {
         curr=delta[t-1][i]+transitions[i][j]+emissions[j][observations[t]];
         if (curr<delta[t][j])
         {
           delta[t][j]=curr;
           psi[t][j]=i;
         }
        }
        //System.out.println("t:"+t+" i:"+psi[t][j]+" j:"+j+" delta:"+delta[t][j]);

      }
    }
    int time=INF;
    for (int i=0;i<N;i++)
    {
      if (delta[T-1][i]<time)
      {
        time=delta[T-1][i];
        bestState[T-1]=i;
      }
    }
    /*
    for (int t=T-2;t>=0;t--)
    {
      bestState[t]=psi[t+1][bestState[t+1]];
    }
    System.out.println("Chosen servers:");
    for (int t=0;t<T;t++)
    {
		System.out.print(bestState[t]+" ");
	}
	System.out.println();
    System.out.println("Individual data transmission times:");
	System.out.print(transitions[0][bestState[0]]+" ");
    for (int t=1;t<T;t++)
    {
		System.out.print(transitions[bestState[t-1]][bestState[t]]+" ");
	}
	System.out.println();
    System.out.println("Individual data processing times:");
    for (int t=0;t<T;t++)
    {
		System.out.print(emissions[bestState[t]][observations[t]]+" ");
	}
	System.out.println();*/
    return time;
  }

  public static void main(String[] args) throws Exception
  {
    Scanner sc=new Scanner(System.in);
    int N=sc.nextInt();
    int O=sc.nextInt();
    int T=sc.nextInt();
    int [][] transitions=new int[N][N];
    int initial []=new int[N];
    int emissions[][]=new int[N][O];
    int observations[]=new int[T];
    
    for (int i=0;i<N;i++)
        for (int j=0;j<N;j++)
        {
          transitions[i][j]=sc.nextInt();
        }
    for (int i=0;i<N;i++)
        initial[i]=transitions[0][i];
    for (int i=0;i<N;i++)
        for (int o=0;o<O;o++)
        {
          emissions[i][o]=sc.nextInt();
        }
    for (int t=0;t<T;t++)
       observations[t]=sc.nextInt();

    System.out.println(hmm_viterbi(transitions,initial,emissions,observations));
  }

}

