/*
 *This program takes an integer as an input and tells you whether it is a perfect
 *number or not. It also gives you an output of the different divisors.
 */

import java.util.*; 
import java.lang.*;
import java.io.*;


public class ProblemD {

    public static void main(String[] args) throws Exception{
        
        //Scanner object for input
        Scanner input = new Scanner(System.in);
        
        //First input for the number of integers to follow
        int numInputs = input.nextInt();
        
        //initialize the position of scanner with respect to line number
        int line = 1;
        
        //iterating over number of inputs
        while (line <= numInputs){
            int num = input.nextInt();      //number input
            
            //****The divisor method + printing the divisors************//
            int sum = 0;                                                //
            for( int i = 1; i < num; i++ ){                             //
                if( (num / (double)i) == (double)(num / i) ){           //
                    sum += i;                                           //
                    System.out.print(i + " ");                          //
                }                                                       //
            }                                                           //
            if(sum == num)                                              //
                System.out.println("YES");                              //
            else                                                        //
                System.out.println("NO");                               //
            //**********************************************************//
            line++;                        //increment line
        }
    }
}
