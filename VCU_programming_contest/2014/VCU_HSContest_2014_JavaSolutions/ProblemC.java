/*
 * This is the sample solution of ProblemC.
 * It will take an input file which represents the dimensions of a
 * list of boxes and return the maximum number of boxes can be used
 * for packing.
 */

import java.util.Scanner;
/**
 *
 * @author cfung
 */
public class ProblemC {

    /**
     * Return 1 if a's dimension if strictly larger than b; return -1 is a's 
     * dimension is strictly smaller than b; elase return 0.
     * @param dim Array representing dimensions of boxes
     * @param a box index a
     * @param b box index b
     * @return larger then 1; smaller then -1; else 0.
     */
    private int dimensionCompare(int[][] dim, int a, int b) {
        if(dim.length < a+1 || dim.length < b+1) {
            System.out.println("Error: Array out of boundary.");
            System.exit(-1);
        }
        
        if(dim[0].length != 3) {
            System.out.println("Error: Need to be in three dimensions.");
            System.exit(-1);
        }
        
        if(dim[a][0]>dim[b][0] && dim[a][1]>dim[b][1] && dim[a][2]>dim[b][2]
                || dim[a][0]>dim[b][0] && dim[a][1]>dim[b][2] && dim[a][2]>dim[b][1]
                || dim[a][0]>dim[b][1] && dim[a][1]>dim[b][0] && dim[a][2]>dim[b][2]
                || dim[a][0]>dim[b][1] && dim[a][1]>dim[b][2] && dim[a][2]>dim[b][0]
                || dim[a][0]>dim[b][2] && dim[a][1]>dim[b][0] && dim[a][2]>dim[b][1]
                || dim[a][0]>dim[b][2] && dim[a][1]>dim[b][1] && dim[a][2]>dim[b][0])
        {
            return 1;
        }

        if(dim[a][0]<dim[b][0] && dim[a][1]<dim[b][1] && dim[a][2]<dim[b][2]
                || dim[a][0]<dim[b][0] && dim[a][1]<dim[b][2] && dim[a][2]<dim[b][1]
                || dim[a][0]<dim[b][1] && dim[a][1]<dim[b][0] && dim[a][2]<dim[b][2]
                || dim[a][0]<dim[b][1] && dim[a][1]<dim[b][2] && dim[a][2]<dim[b][0]
                || dim[a][0]<dim[b][2] && dim[a][1]<dim[b][0] && dim[a][2]<dim[b][1]
                || dim[a][0]<dim[b][2] && dim[a][1]<dim[b][1] && dim[a][2]<dim[b][0])
        {
            return -1;
        }
        return 0;
    }
    /**
     * Return the maximum number of boxes can be used
     * to wrap each other.
     * @param boxes
     * @return the maximum number of boxes can be used
     */
    public int getMaxBoxes(int[][] boxes) {
        int[] maxBoxes = new int[boxes.length];
        int max = -1;
        // initialize
        for(int i=0; i<maxBoxes.length; i++) {
            maxBoxes[i] = 1;
        }
        
        for(int idx=0; idx<maxBoxes.length; idx++) {
            // For all smaller boxes, this box can be wraped on top
            for(int j=0; j<idx; j++) {
                if(dimensionCompare(boxes, j, idx) < 0) {
                    maxBoxes[idx] = Math.max(maxBoxes[j]+1, maxBoxes[idx]);
                }
            }
            
            // For all larger boxes, this box can be wraped in
            boolean changed=true;
            while(changed){
                changed=false;
                for(int k=0; k<=idx; k++) {
                    for(int j=0; j<=idx; j++)
                    if(dimensionCompare(boxes, k, j) > 0) {
                        int rst = Math.max(maxBoxes[k], maxBoxes[j]+1);
                        if(rst > maxBoxes[k]) {
                            maxBoxes[k] = rst;
                            changed=true;
                        }
                            
                    }
                }
            }

        }

        // Return the maximum boxes
        for(int n=0; n<maxBoxes.length; n++) {
            if(maxBoxes[n] > max) {
                max = maxBoxes[n];
            }
        }
        return max;
    }
    
    public void solve() {
        // Read the input file
        String line;
        int numBoxes=0;
        int[][] boxes;
        String[] temp;
        String[] dims;
        Scanner insc = new Scanner(System.in);

                numBoxes = insc.nextInt();
                boxes = new int[numBoxes][3];

                for(int i=0; i<numBoxes; i++) {
                    // parse dimentions
                    for(int j=0; j<3; j++) {
                        boxes[i][j] = insc.nextInt();
                    }
                }

                System.out.println(getMaxBoxes(boxes));
            



        // Solve the problem

        // Print the results
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ProblemC p;
        p = new ProblemC();
        p.solve();
    }
}
