import java.util.Scanner;

public class ProblemH {
  public static void main(String args[])  {
	Scanner in = new Scanner(System.in);
       int TT = in.nextInt();
       while(TT>0){
         	TT--;
	 	int n = in.nextInt();
		int[] x = new int[n];
		for (int i = 0; i < n; i++) {
			x[i] = in.nextInt();
		}
		while (n>2){
			for (int i = 0; i < n; i++) {
				if (i>n-i-1)
					break;
				x[i] = x[i]+ x[n-i-1];
			}
			n = (int)Math.ceil((double)n/(double)2);
		}
		if (x[0] >= x[1])
			System.out.println("Ana");
	 	else
			System.out.println("Voyo");
       }
   }
}
