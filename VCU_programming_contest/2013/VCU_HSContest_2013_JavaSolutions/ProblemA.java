import java.util.*;
public class ProblemA
{
	public static void main(String args[]) throws Exception
	{
		int numInt = 7;
		int numLines;
		int i,j;
	    int flag1=0,flag2=0,flag3=0,flag4=0,flag5=0;
		String []results;
	
		Scanner cin = new Scanner(System.in);
		numLines = cin.nextInt();
		results = new String[numLines];	

		for(i=0; i<numLines; i++)
		{
			for(j=0; j<numInt; j++)
			{
				int tmp = cin.nextInt();
				switch(tmp)
				{
					case 1:
						  flag1++;
						  continue;
					case 2:
						  flag2++;
						  continue;
					case 3:
						  flag3++;
					      continue;
					case 4:
						  flag4++;
					      continue;
					case 5:
						  flag5++;
					      continue;
				}
			}
			if ((flag1>=1)&&(flag2>=1)&&(flag3>=1)&&(flag4>=1)&&(flag5>=1))
				results[i] = "Bet";
			else
				results[i] = "Fold";
			flag1=flag2=flag3=flag4=flag5=0;
			
		}
		
		for(i=0; i<numLines; i++)
			System.out.println(results[i]);
	}
}
