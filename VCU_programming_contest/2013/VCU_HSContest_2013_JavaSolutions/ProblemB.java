import java.util.*;
import java.io.File;
import java.io.IOException;

class ProblemB
{
	static void spiralPrint(int[][] mat, int row, int col)						//Method to print the matrix in a clockwise spiral pattern
		{
			int m = row; 															// rows
			int n = col; 															// cols
			int i,j = 0;
			int trow = 0, tcol = 0;													//rows and cols already printed
			int rowNo = 0, colNo = 0;
			int count = row*col;													//count of total number of elements of the matrix to be printed

			while(true)																//recursive spiral printing till end of count
			{
				for(colNo = tcol;colNo <= (n - tcol - 1);colNo++)					//printing from top left to top right
				{
					//System.out.print("\n top left to top right");
					System.out.print(mat[rowNo][colNo]+" ");
					count--;
					if(count == 0)
					return;
				}
				colNo--;
				tcol = colNo;

				for(rowNo = trow + 1; rowNo <= (m - trow - 1);rowNo++) 				//printing from top right to bottom right
				{
					//System.out.print("\n top right to bottom right");
					System.out.print(mat[rowNo][colNo]+" ");
					count--;
					if(count == 0)
					return;
				}
				rowNo--;
				trow = rowNo;

				for(colNo = tcol - 1;colNo >= (n - tcol - 1);colNo--) 				//printing from bottom right to bottom left
				{
					//System.out.print("\n bottom right to bottom left");
					System.out.print(mat[rowNo][colNo]+" ");
					count--;
					if(count == 0)
					return;
				}
				colNo++;
				tcol = colNo;

				for(rowNo = trow - 1; rowNo > (m - trow - 1);rowNo--)  				//printing from bottom left to top left
				{
					//System.out.print("\n bottom left to top left");
					System.out.print(mat[rowNo][colNo]+" ");
					count--;
					if(count == 0)
					return;
				}
				rowNo++;
				trow = rowNo;
				tcol++;
			}
		}

	public static void main(String args[]) throws IOException   				//main method
	{

//		String location = args[0];
		int row=1,col=0;

		Scanner myScanner = new Scanner(System.in);
		//String FirstLine = myScanner.nextLine();
		int len = myScanner.nextInt();

		//System.out.print("\n First Line: ");

		//System.out.print(len);

		int[][] mat = new int [len][len];

		for(int i=0;i<len;i++)
				{
					for(int j=0;j<len;j++)
					{
						mat[i][j] = myScanner.nextInt();								//adding elements into the matrix

						//System.out.print(i+" "+j);
						//System.out.print(mat[i][j]+" "); //printing for check
					}
				}

		//System.out.println();
		spiralPrint(mat,len,len);
	}
}
