import java.util.*;
import java.lang.*;

class DisjointSet
{
  int ID; 
  int rank; 
  DisjointSet parent; 
  
  DisjointSet(int nodeID){ID=nodeID;rank=0;parent=this;}

  public int getID(){return ID;};

  public static void Link(DisjointSet x,DisjointSet y) 
  {
	if (x.rank>y.rank)
	{
	  y.parent=x;
	}
	else
	{
	  x.parent=y;
	  if (x.rank==y.rank)
		 y.rank++;
	}
  } 
  
  public static DisjointSet FindSet(DisjointSet x)
  {
	if (x!=x.parent)
	{
	  x.parent=FindSet(x.parent);
	}
	return x.parent;
  }
  
  public static void Union(DisjointSet x,DisjointSet y)
  {
	Link(FindSet(x),FindSet(y));
  }

}


public class ProblemI
{

 	public static void main(String args[]) throws Exception
	{
		int wormholes;
		int i;
		int a,b;
		DisjointSet current,earth; // current is where you are and earth is home
		DisjointSet sa,sb; 

		Scanner cin = new Scanner(System.in);// gets the input
		wormholes = cin.nextInt();// gets the number of wormholes

		HashMap<Integer,DisjointSet> vertexMap=new HashMap<Integer,DisjointSet>();


		for (i=1;i<=wormholes;i++)
		{
		  a=cin.nextInt();
		  b=cin.nextInt();
		  
		  sa=vertexMap.get(new java.lang.Integer(a));
		  if (sa==null)
		  {
			sa=new DisjointSet(a);
			vertexMap.put(new java.lang.Integer(a),sa);
		  }
		  
		  sb=vertexMap.get(new java.lang.Integer(b));
		  if (sb==null)
		  {
			sb=new DisjointSet(b);
			vertexMap.put(new java.lang.Integer(b),sb);
		  }
		  
		  DisjointSet.Union(sa,sb);
		} // puts all the wormholes in a disjoint set map

		int m=cin.nextInt(); // m is the number of problems
		for (i=1;i<=m;i++)
		{

			a=cin.nextInt();
			b=cin.nextInt();
			
			current=vertexMap.get(new java.lang.Integer(a));
			earth=vertexMap.get(new java.lang.Integer(b));

			if (DisjointSet.FindSet(earth).getID()==DisjointSet.FindSet(current).getID())
			{
			  System.out.println("ENGAGE WORMHOLE TRAVEL PLAN");
			}
			else
			{
			  System.out.println("COMPUTER, LOCATE NEAREST JUNKYARD");
			}
		}
	}
}
