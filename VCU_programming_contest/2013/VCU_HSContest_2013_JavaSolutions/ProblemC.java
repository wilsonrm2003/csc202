import java.util.Arrays;
import java.util.Scanner;

//  Basic idea

//  Each corner of a previously fenced area (not on the outside edge of the field)
//  must have 1 and only one new fence connected to it.

//  If you draw all horizontal (or vertical) lines from each corner
//  you get a valid - but maybe not minimum - solution.
//  Savings can be made when rectangles line up vertically (or horizontally)
//
//  First connect edges that are lined up vertically.
//  Some of these may need to be erased because a horizontal edge may need to be connected from
//  another corner.  If so, erase that vertical edge.

//  Connect the remaining corners horizontally and count the new rectangles

// field array data values
//   0         open field
//   -1 to -8  corners of original rectangles each rectangle has a different value some may share
//  -11 to -18 edges of original rectangles
//  -21 to -28 insides of original rectangles
//  -29        shared corner
//  -59        new vertical edges
//  -61        new horizontal edges

public class ProblemC {


	public static void main(String[] args) {
		int i,j, i1, j1, i2, j2, rc, re, ri;
		int toprc = 0;
		int topin;
		int tmp, arraycount=0;
		boolean more, remove;
		
//		int[] row = new int[20];
		int[] col = new int[20];
		int[][]field = new int[101][101];
		
//  Init field to all zeros
		for (i=0; i<101; i++)
			for (j=0; j<101; j++)
				field[i][j] = 0;
		
//Get input	
		rc = -1;		// input rectangle corner count
		re = -11;		// input rectangle edge count
		ri = -21;		// input rectangle interior count
		
		Scanner s = new Scanner(System.in);
		topin = s.nextInt();
		
		for (int k=0; k<topin; k++)
		{
			i1 = s.nextInt();
			j1 = s.nextInt();
			i2 = s.nextInt();
			j2 = s.nextInt();

			// remember row and col to check for duplicates later
//			row[toprc] = i1;			// don't need the row unless drawing the other way
//			row[toprc+1] = i2;
			col[toprc] = j1;
			col[toprc+1] = j2;
			toprc = toprc + 2;
			
			//  Put in order top left to bottom right
			if (i1 > i2)
			{   i = i1;   i1 = i2;   i2 = i;}
			if (j1 > j2)
			{   j = j1;   j1 = j2;   j2 = j;}
			
			// Set corners
			if (field[i1][j1] == 0) 
				field[i1][j1] = rc;
			else
				field[i1][j1] = -29;
			
			if (field[i1][j2] == 0) 
				field[i1][j2] = rc;
			else
				field[i1][j2] = -29;
			
			if (field[i2][j1] == 0) 
				field[i2][j1] = rc;
			else
				field[i2][j1] = -29;
			
			if (field[i2][j2] == 0) 
				field[i2][j2] = rc;
			else
				field[i2][j2] = -29;
			
			rc--;	
			
			//  Draw edges
			for (i=i1+1; i<i2; i++)
			{
				field[i][j1] = re;
				field[i][j2] = re;
			}
			for (j=j1+1; j<j2; j++)
			{
				field[i1][j] = re;
				field[i2][j] = re;
			}
			re--;	
		
			// Fill inside
			for (i=i1+1; i<i2; i++)
				for (j=j1+1; j<j2; j++)
					field[i][j] = ri;
			ri--;
		}
			
		// Draw edge around outside
		for (i=0; i<101; i++)
		{
			if (field[i][0] == 0) field[i][0] = -51;
			if (field[i][100] == 0) field[i][100] = -51;
			if (field[0][i] == 0) field[0][i] = -51;
			if (field[100][i] == 0) field[100][i] = -51;
		}

		//  Sort lists of row and col used as corners
//		Arrays.sort(row, 0, toprc);
		Arrays.sort(col, 0, toprc);

// set vertical edges on same column
		for (j=0; j<toprc-1; j++)
		{
			if (col[j] == col[j+1])			//see if same col appears twice
			{
				j1 = col[j];
				i = 0;
				while (i < 101)           // for entire column
				{
					
					// Find next corner in col with open space below
					while (i<100 && (field[i][j1] >= 0 || field[i][j1] < -10 || field[i+1][j1] != 0)) 
						i++;
				
					i1 = i + 1;					//See if clear path to next corner
					while (i1 < 101 && field[i1][j1] == 0)
						i1++;
					if (i1 < 101 && field[i1][j1] < 0 && field[i1][j1] > -10)     //fill it in 
						for (i2=i+1; i2<i1; i2++)
							field[i2][j1] = -59;
					i = i1;
				}
			}		
		}	
		
// Remove vertical edges that are redundant
		more = true;
		while (more)				//  You need to repeat until nothing is removed 
		{
			more = false;
			for (j=0; j<toprc-1; j++)
			{
				if (col[j] == col[j+1])			//see if same col appears twice
				{
					j1 = col[j];
					i = 0;
					while (i < 101)           // for entire column
					{
					
						// Find first part of vertical line
						while (i<101 && field[i][j1] != -59)
							i++;
					
						if (i < 101)
						{
							i1 = i - 1;					//i1 is top of line
							while (field[i][j1] == -59)
								i++;
							//i is bottom of vertical line
							// See if top of line is will be connected left or right
							remove = false;
							j2 = j1 - 1;
							while (field[i1][j2] == 0)
								j2--;
							if (field[i1][j2] < 0 && field[i1][j2] > -10)
							   remove = true;

							j2 = j1 + 1;
							while (field[i1][j2] == 0)
								j2++;
							if (field[i1][j2] < 0 && field[i1][j2] > -10)
								remove = true;

							// See if bottom of line is will be connected left or right
							j2 = j1 - 1;
							while (field[i][j2] == 0)
								j2--;
							if (field[i][j2] < 0 && field[i][j2] > -10)
								remove = true;
							
							j2 = j1 + 1;
							while (field[i][j2] == 0)
								j2++;
							if (field[i][j2] < 0 && field[i][j2] > -10)
								remove = true;
							
							if (remove)		//if either end is required then remove vertical line
							{
								i1++;
								while (field[i1][j1] == -59)
								{
									field[i1][j1] = 0;
									i1++;
								}	
								more = true;
							}
						}
					}
				}
			}		
		}	
		
		
		
// Draw horizontal lines to make rectangles
		for (i=1; i<100; i++)
			for (j=1; j<100; j++)
			{
				tmp = field[i][j];
				if (tmp < 0 && tmp > -10)       // got corner
				{
					if (!(field[i-1][j] < -50 || field[i+1][j] < -50 || field[i][j-1] < -50 || field[i][j+1] < -50))
					{
						//Vertical line is not already connected, check right, then left
						if (field[i][j+1] == 0)
						{
							j1 = j + 1;
							while (field[i][j1] == 0)
							{
								field[i][j1] = -61;
								j1++;
							}
						}
						else if (field[i][j-1] == 0)
						{
							j1 = j - 1;
							while (field[i][j1] == 0)
							{
								field[i][j1] = -61;
								j1--;
							}
						}
					}
				}											  
			}	
	
// Fill and count rectangles
		for (i=1; i<100; i++)
			for (j=1; j<100; j++)
			{
				if (field[i][j] == 0)
					if (field[i][j-1] > 0) field[i][j] = field[i][j-1];
					else if (field[i-1][j] > 0) field[i][j] = field[i-1][j];
					else {
						arraycount++;
						field[i][j] = arraycount;
					}
			}	

		
// Print corner of field for debugging
/*
		for (i=0; i<40; i++)
		{	for (j=0; j<40; j++)
			System.out.format("%4d",field[i][j]);
			System.out.println();
		}
 */
		
		System.out.println(arraycount);

		
	}
}
