
//package highschoolhex;
import java.io.*; import java.util.*; import java.io.*;

public class ProblemF {
    private static int side = 3;
    private static String[][] g;
    private static int globalCount = 0;
    
    
    
    public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		side = cin.nextInt();
		
        g = new String[2*side+1][4*side+1];
        drawHex();
        int a = side;
        countStructure(side);
        System.out.println(globalCount);
    }
    
    //////////////////////////////////////////////////////
    public static void drawHex(){
        int x, level=0;
        for (int i=0; i<side+1; i++){
            x = 0;
            for (int j=0; j<(side-level); j++){
                g[i][x] = ".";
                x++;
            }
            for (int j=0; j < (2*(side+level))+1; j++){
                if ((j%2) == 0) g[i][x]= "x";
                else g[i][x]=".";
                x++;
            }
            for (int j=0; j<(side-level); j++){
                g[i][x]=".";
                x++;
            }
            level++;
        }
        
        int downLevel = side-1;
        for (int i=side+1; i<2*side+1; i++){
            x=0;
            for (int j=0; j<(level-side); j++){
                g[i][x] = ".";
                x++;
            }
            
            for (int j=0; j<(2*(side+downLevel))+1; j++){
                if ((j%2) == 0) g[i][x]= "x";
                else g[i][x]=".";
                x++;
            }
            
            for (int j=0; j<(level-side); j++){
                g[i][x] = ".";
                x++;
            }
            
            downLevel--;
            level++;
        }
        
        for (int i=0; i< g.length; i++){
            for (int j=0; j<g[i].length; j++){
                //System.out.printf("%s",g[i][j]);
            }
            //System.out.printf("\n");
        }
        //System.out.println("-------------");
    }
    ////////////////////////////////////////////////////////////
    
    public static void countStructure(int a){
        String[][] mat = new String[a+1][2*a+1];
        int level = 0;
        for (int i=0; i<mat.length; i++){
            int x=0;
            for (int j=0; j<a-level; j++){
                mat[i][x] = ".";
                x++;
            }
            for (int j=0; j<(2*level)+1; j++){
                if (j%2 == 0) mat[i][x] = "x";
                else mat[i][x] = ".";
                x++;
            }
            for (int j=0; j<a-level; j++){
                mat[i][x] = ".";
                x++;
            }
            level++;
        }
        for (int i=0; i<mat.length; i++){
            for (int j=0; j<mat[i].length; j++){
                //System.out.printf("%s",mat[i][j]);
            }
            //System.out.printf("\n");
        }
        int count = 0; boolean set;
        for (int i=0; i< (2*side+1)-(mat.length)+1; i++){
            //System.out.printf("---%d\n", (2*side+1)-(mat.length)+1);
            for (int j=0; j< (g[i].length-(2*a+1))+1; j++){
                //System.out.printf("---%d\n", g[i].length-(2*a+1) +1);
                set = true;
                for (int k=0; k<a+1; k++){
                    //System.out.printf("---%d\n", a+1);
                    for (int l=0; l<2*a+1; l++){
                        //System.out.printf("---%d\n", 2*a+1);
                        //System.out.printf("%d %d %d %d\n", i+k, j+l, j, l);
                        //System.out.println(2*a+1);
                        if(mat[k][l] == "x" && g[i+k][j+l] != mat[k][l])
                            set = false;
                    }
                }
                if (set == true) count++;
            }
        }
        //System.out.printf("\nsub_count: %d\n\n", 2*count);
        globalCount = globalCount + (2*count);
    }    
}
