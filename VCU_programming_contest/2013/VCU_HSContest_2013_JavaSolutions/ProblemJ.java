import java.util.*;
public class ProblemJ
{
	public static int steps(int a, int b, int c, int d)
	{
		int n=0;
		int a0;
		while(true)
		{
			if (a==b && a==c && a==d)
				return n;
			a0 = a;
			a = Math.abs(b-a);
			b = Math.abs(c-b);
			c = Math.abs(d-c);
			d = Math.abs(a0-d);
			n++;
		}
	}

	public static int search(int steplimit, int intlimit)
	{
		int i,j,k,l,n;
		for(i=0; i<=intlimit; i++)
			for(j=0; j<=intlimit; j++)
				for(k=0; k<=intlimit; k++)
					for(l=0; l<=intlimit; l++)
						if(i==intlimit || j==intlimit || k==intlimit || l==intlimit)
							if((n=steps(i,j,k,l)) >= steplimit)
								return n;

		return 0;
	}

	public static void main(String args[]) throws Exception
	{
		int nmax;
		int nsteps;
		int found;
		
		Scanner cin = new Scanner(System.in);
		nsteps = cin.nextInt();

		found = 0;
		nmax = 1;
		while (found==0)
		{
			found = search(nsteps, nmax);
			if (found>0)
				System.out.println(nmax);
			else nmax++;
		}
	}	
}