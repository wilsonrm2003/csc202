import java.io.*;
import java.*;
import java.util.Scanner;  //for input

public class ProblemG {

	private static Scanner scanner = new Scanner( System.in );


	public static void main(String [] args) throws Exception {

		// read in input
		int numOfSets = scanner.nextInt();
		int i;
		for (i=1;i<=numOfSets;i++)
		{
			solveSet();
		}
	}

	public static void solveSet() throws Exception {
			int numOfZones = scanner.nextInt();
			int zoneRects[][] = new int[numOfZones][];

			for (int i=0; i<numOfZones; i++) {
				zoneRects[i] = new int[4];
				for (int j=0; j<4; j++) {
					zoneRects[i][j] = scanner.nextInt();
				}
			}

			boolean overlap = findOverlaps(numOfZones, zoneRects);


			if (overlap)
				System.out.println("Overlap");
			else
				System.out.println("No Overlap");
	}


	public static boolean findOverlaps(int numZone, int zoneRects[][]) {

		int iX1, iX2, iY1, iY2;
		int jX1, jX2, jY1, jY2;

		for (int i=0; i<numZone; i++) {
			iX1=Math.min(zoneRects[i][0],zoneRects[i][2]);
			iY1=Math.min(zoneRects[i][1],zoneRects[i][3]);
			iX2=Math.max(zoneRects[i][0],zoneRects[i][2]);
			iY2=Math.max(zoneRects[i][1],zoneRects[i][3]);
			for (int j=0; j<numZone; j++) {
				jX1=Math.min(zoneRects[j][0],zoneRects[j][2]);
				jY1=Math.min(zoneRects[j][1],zoneRects[j][3]);
				jX2=Math.max(zoneRects[j][0],zoneRects[j][2]);
				jY2=Math.max(zoneRects[j][1],zoneRects[j][3]);
				if ((i!=j) && !(
					(iX1<jX1 && iX2<jX1) ||
					(iX1>jX2 && iX2>jX2) || 
					(iY1<jY1 && iY2<jY1) ||
					(iY1>jY2 && iY2>jY2) 
				    ))
				{
					return true;
				}
			}
		}
		return false;
	}
}
