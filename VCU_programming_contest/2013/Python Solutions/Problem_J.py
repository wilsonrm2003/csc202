'''
In the world of Similarity people are judged by the
 number of poogles that they own. The more poogles 
 you have the lower your status. However, it is hard
  to get rid of poogles – no one will take them from 
  you, and if you try to abandon them they just fly 
  right back to you. When a person dies, their poogles
   become the property of their nearest blood relative.
In order to right this inequality, the government 
(who else would right an inequality?) has established 
the Ministry for Poogle Redistribution and Care. The 
Ministry has a poogle redistribution plan. If you 
apply you can participate in redistribution. However, 
the Ministry sometimes also forces people with just 
a few (or no) poogles to participate.
The Ministry takes people in groups of 4 for redistribution.
 The 4 people, call them A, B, C, and D form a circle 
 around the minister in charge. The minister then calculates
    |poogles A has – poogles B has|
and gives that number of poogles to A. This process is repeated
 for B and C, C and D, and D and A. This completes one cycle of
  redistribution. The cycles are repeated until everyone has
   the same number of poogles.
The Most High Minister is interested in the relationship 
between the number of poogles people have and the number 
of cycles needed to complete redistribution. You are to 
write a program that will, given the number of cycles, 
tell the Most High Minister the smallest maximum number 
of poogles that one person must have in order to need 
that number of cycles.
For example, consider having 3 cycles. Then you must 
have at least one person with 1 poogle. 
If you have A=1, B=0, C=0, D=0, then you will get:
after cycle 1: A=1, B=0,C=0, D=1 
after cycle 2: A=1, B=0, C=1, D=0 
after cycle 3: A=1, B=1, C=1, D=1
Input: There will be one line of input containing 1
    integer giving the number of cycles.
Output: You should output a single integer giving the 
    smallest maximum number of poogles that someone must have.
'''
split_contents = []
with open("Problem_J_Input.txt", 'r') as f:
    contents = f.readlines()

for i in range(0,len(contents)):
    split_contents += contents[i].split()

for i in range(0,len(split_contents)):
    starting_poogles = int(split_contents[i])
    # find a way to distrubute the poogles
    # do the absolute value
    # do abs val until all have had equal poogle at least once
