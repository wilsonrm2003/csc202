'''
Your spaceship fell into a time-space anomaly and you have ended up in Theta-Umber 
    sector. Luckily your navigator realized where you were by recognizing the unusual 
    purple haze atmosphere of the planet Arodz. With this information and your computers
    complete database of all star systems and wormholes you think you can get home.
    A wormhole is a two-way tunnel through space-time connecting two points in space,
    each in a different star system.
Each time you travel through a wormhole it takes a toll on your shields. The longer
    the wormhole, the bigger the toll. If your shields give way then you are toast. 
    Chief Engineer Ghosh has reported that the shields were damaged when you fell through 
    the anomaly. He recommends that you travel through no wormhole longer than 10 light 
    years. You may consider the universe cut into star systems. Each star system has a 
    corresponding ID. You may easily travel within a star system without any real wear 
    and tear on your spaceship. You cannot travel to another star system without going 
    through a wormhole.
So, can you get home? Or are you going to have to travel the universe looking for 
    a space junkyard where you can get the pieces to repair your shield?

Input: The first line of input is a single integer n giving the number of wormholes
 in the universe that are of length less than 10 light years. The next n lines each
  consist of two integers separated by a space. The first integer is the ID of the
   star system where the wormhole starts. The second is the ID of the end of the 
   wormhole.
   The next line contains the number of problems to solve, m. Each of the 
   following m lines contains two integers separated by a space, defining the
    problem. The first is the ID of the current star system you are in. The second
     is the ID of the star system you call home. You are to determine if home can 
     be reached from the current position. 

Output:
For each of the m problems, you should print a single line. If you find a way home, 
then print out the line “ENGAGE WORMHOLE TRAVEL PLAN”. If you do not find a way home, 
then print out the line “COMPUTER, LOCATE NEAREST JUNKYARD”
'''

split_contents = []
with open("Problem_I_Input.txt", 'r') as f:
    contents = f.readlines()

for i in range(0,len(contents)):
    split_contents += contents[i].split()

wormholes = int(contents[0])
problems = int(contents[wormholes+1])

for i in range(wormholes+2,problems+wormholes+2):
    problem = contents[i].split()
    homeID = int(problem[1])
    where = int(problem[0])
    for i in range(1, wormholes*2):
        if homeID == int(split_contents[i]) and where == int(split_contents[i]):
            #find way to evaluate wormhole ID better
            safe = True
            break
        else:
            safe = False
    if safe:
        print("ENGAGE WORMHOLE TRAVEL PLAN")
    else: 
        print("COMPUTER, LOCATE NEAREST JUNKYARD")





    
    




