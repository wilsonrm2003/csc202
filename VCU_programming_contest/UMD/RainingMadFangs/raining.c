#include <stdio.h>
#include <stdbool.h>

#define MAXLEN 101
#define DICTLEN 20
#define TRUE 1
#define FALSE

// pascal solution converted into C
int w;
int i;
int j;
char target[100];
int numberWordsPicked;
char dict[101][20];
int solution[100][20];
bool foundOneAnagram;  
int avail[100];
int firstAvailWord;
bool match; 
char c;
int numWords;
int index;

int solve(char letters, int firstAvailWord, int numberWordsPicked);

int solve(char letters, int firstAvailWord, int numberWordsPicked) {
    int i = 1;
    while((i <= 26) && (target[i] = 0)) {
        i++;
    }

    if(i == 27) {
        for(i = 1; i < numberWordsPicked; i++){
            j = 1; 
            while ((dict[solution[i]][j] != ' ') && (j <= 10)) {
                printf(dict[solution[i]][j]);
                j++;
            }
            printf(" ");
            foundOneAnagram = true;
        }
        printf("\n");
    }

    else {
        for (w = firstAvailWord; w < numWords; w++){
            i = 1;
            for (i = 1; i < 26; i++){
                avail[i] = target[i]; 
            }
            i = 1;
            match = true;
            while ((dict[w, i] != ' ') && (match)) {
                if(('a' <= dict[w,i]) && (dict[w,i] <= 'z')) {
                    index = ord(dict[w,i]) - ord('a') + 1;
                    if (avail[index] < 0) {
                        match = false;
                    }
                }
                i++;
            }
            if (match) {
                solution[numberWordsPicked+1] = w;
                solve(avail,w+1,numberWordsPicked);
            }
        }
    }
}

int main() {
    
    for (i =0; i < 26; i++){
        target[i] = 0;
    }
    
    while (!eoln(input)){//find eoln equivalent
        scanf("%c", c);
        if (('a' <= c) && (c <= 'z')) {
            index = ord(c) - ord('a') + 1; // find ord equivalent
            target[index] = target[index] + 1;
        }
    }
    
    w = 1;
    
    while (getchar() != EOF) { // find eof means
        for (i = 1; i < 10; i++){
            dict[w,i] = ' ';
        }
        
        i=1;
        
        while (!eoln(input)) {
            scanf(dict[w][i]); // thats not right
            i++;
        }
        
        for( i =1; i < 26; i++) {
            avail[i] = target[i]; 
        } 
        
        match = true;
        
        i = 1;
        
        while((dict[w,i] !=  ' ') && (match)){
        
            if ('a' <= dict[w,i]) && (dict[w,i] <= 'z') {
                index = ord(dict[w,i]) - ord('a') + 1;
                avail[index] = avail[index] - 1;
        
                if (avail[index] < 0){
		            match := false;
                }
                i++;
            }
        }
        
        if (match) {
            w++;
        }
    }
    numWords = w;
    solve(target, 1, 0);
    
    if (foundOneAnagram) {
        printf("No anagrams for this sentence\n");
    } 
}