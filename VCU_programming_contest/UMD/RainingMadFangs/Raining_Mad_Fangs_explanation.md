Write a program that outputs all sentences that are anagrams of the input
sentence using only words from the supplied dictionary. For sentence to
be a valid anagram, it must use all of the letters in the input sentence
exactly once.

Input Format

The first line is a sentence. The remaining lines are the words of a dictionary, 
entered one word per line. Other than the spaces appearing between words of the 
first line, the input will consist only of lower-case alphabetic symbols (`a'--`z').
All words will be no more than 10 characters long. There will be at most 5 words on 
the first line and no more than 20 lines after that (each of which contain one word).

Output Format

You must print all combinations of words from the dictionary that are anagrams of the 
first sentence. For example, ``Raining Mad Fangs" is an anagram of ``Finding Anagrams''. 
For each combination of words, you don't have to print them our in every possible order. 
You may may only use each word from the dictionary once per anagram. However, the same 
word may appear multiple times in the dictionary.

If no anagrams are possible from the input sentence, your program should print "No anagrams for this sentence".

Sample Input:
finding anagrams
admiring
afar
drama
fan
fans
fangs
mad
minding
nag
race
rain
quick
raining
signing
soup

Sample output:
admiring fans nag 
drama fan signing 
fangs mad raining