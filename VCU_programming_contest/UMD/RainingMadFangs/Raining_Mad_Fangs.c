#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXLINE 1000 
#define MAXWORDS 20
#define WORDLEN 10

/* accgetline: get line into s, return length */
int accgetline(char s[], int lim)
{
    int c, i;

    i = 0;
    while (--lim > 0 && (c = getchar()) != EOF && c != '\n')
        s[i++] = c;
    if (c == '\n')
        s[i++] = c;
    s[i] = '\0';
    return i;
}

int is_anagram(char str1[], char str2[]) {
    int first[26] = {0}, second[26] = {0}, i = 0; 

    while (str1[i] != '\0') {
        first[str1[i] - 'a']++;
        i++;
    }

    i = 0;

    while (str2[i] != '\0') {
        second[str2[i] - 'a']++;
        i++;
    }

    for (i = 0; i < 26; i++)
        if (first[i] != second[i])
            return 0;

    return 1;
}

int readfile(char dict[][WORDLEN]){
    FILE* fp = "inp1.dat", "r";
    // FILE* fp = "inp2.dat", "r";
    // FILE* fp = "inp3.dat", "r";
    // FILE* fp = "inp4.dat", "r";
    if (!fp) {
        printf("Error opening file");
        exit(EXIT_FAILURE);
    }

    int i = 1; // start at not the first line because the first line is for the sentence
    while(scanf(fp, "%s", dict[i]) != EOF) {
        i++;
    }

    fpclose(fp);
    
    return i;
}

int main() {
    
    char sentence[MAXLINE];
    accgetline(sentence, MAXLINE);

    char word[MAXLINE];
    char *dict[MAXWORDS];
    int i = 0;

    
    while (i < MAXWORDS) {
        accgetline(word, MAXLINE);
        dict[i] = word;
        i++;
    }
    
    
    i = 0;
    /*
    char *dict[] = {"admiring", "afar", "drama", "fan", "fans" , "fangs" , "mad", 
    "minding", "nag", "race", "rain", "quick", "raining", "signing", "soup"}; // input 2 

   char *dict[MAXLINE] = {"the", "quick", "brown", "fox", 
    "jumped", "over", "the", "lazy", "dogs"}; // input 3 
    */

    int dictsize = sizeof(dict)/sizeof(dict[0]);

    
    while (i < dictsize) {
        printf("%s ", dict[i]);
        i++;
    }
    
    
    char *anagram[MAXLINE];
    char *anagram2[MAXLINE];

    int notanagram = 0; 

    for (i = 0; i < dictsize; i++) {
        strcat(anagram, dict[i]);
        strcat(anagram, " ");

        if (is_anagram(sentence, anagram)) {
            printf("%s\n", anagram);
        } // should work for input 3 
        if (i < dictsize - 1) {
            strcat(anagram2, dict[i]);
            strcat(anagram2, " ");
            strcat(anagram2, dict[i+1]);
            if (is_anagram(sentence, anagram2)){
                printf("%s\n", anagram2);
            }
            memset(anagram2, 0, MAXLINE);
        }
    }
    printf("dict: %s", &dict);
    printf("anagram: %s", &anagram);

    /*
    char test[] = "The quick brown fox jumped over the lazy dogs";
    if(is_anagram(sentence, test)) {
        prinft("Test sucess");
    }
    // printf("No anagrams for this sentence");
    
   // make a for statement that adds words togehter until they're the same number of chars as sentence 
    
    for (i = 0; i < dictsize; i++) {
        if (is_anagram(sentence, dict[i])) {
            printf("%s", dict[i]);
        }
        char *test = "";
        if (i < MAXWORDS - 1){ 
            strcat(&test, dict[i+1]);
            printf("%s", test);
        }
    }
    */


}