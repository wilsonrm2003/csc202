// this code contains modified lines from https://github.com/arthurtng/anagrams

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#define BUFFERSIZE 130000
#define WORDSIZE 50

int readfile(char wordslist[][WORDSIZE]);
bool isanagram(char* word1, char* word2);
void printanagram(char wordslist[][WORDSIZE], int max, char* word);
bool atLeastOne;  
char *sentence;

int main(int argc, char* argv[])
{
   /*
   if (argc != 2){
     fprintf(stderr, "Incorrect usage, use the following syntax: ./test <word> \n");
     exit(EXIT_FAILURE);
   }
   
   
   if ((int)strlen(argv[1]) >= WORDSIZE){
     fprintf(stderr, "Please enter a word less than 50 characters long\n");
     exit(EXIT_FAILURE);
   }
   */
   
   char wordslist[BUFFERSIZE][WORDSIZE];      
   int max = readfile(wordslist);
   
   printanagram(wordslist, max, argv[1]);
   if(!atLeastOne) {
      printf("No anagram for this sentence");  
   }
       
   return 0;
}

int readfile(char wordslist[][WORDSIZE])
{
   FILE* fp = fopen("inp2.dat", "r");   
   if(!fp){
      fprintf(stderr, "Cannot read from dictionary file\n");
      exit(EXIT_FAILURE);
   }
   
   fscanf(fp, "%s", sentence);

   int i = 1; // start word list at one because first line is sentence 
   while(fscanf(fp, "%s", wordslist[i]) != EOF){
      i++;
   }
      
   fclose(fp);
   
   return i;
}

bool isanagram(char* word1, char* word2)
{
   if (strcmp(word1, word2) == 0 || strlen(word1) != strlen(word2)){
      return 0;
   }
   
   char temp[WORDSIZE];
   int idx = 0;
   strcpy(temp, word2);
   
   int count = 0;
   for (int i=0; i<(int)strlen(word1); i++){
      for (int j=0; j<(int)strlen(word2); j++){
         if (word1[i] == temp[j]){
            count++;
            idx = j;            
         }
      }
      if (count == 0){
         return false;
      }
      count = 0;
      temp[idx] = '0'; 
   }
   return true;
}

void printanagram(char wordslist[][WORDSIZE], int max, char* word)
{
   for (int i = 0; i < max; i++){
      if (isanagram(wordslist[i], sentence) && strcmp(wordslist[i], sentence) != 0){
         printf("%s\n", wordslist[i]);
         atLeastOne = true;
      }
   }
}
