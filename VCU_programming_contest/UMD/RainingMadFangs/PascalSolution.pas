program anagram;

type
    letters = array[1..26] of integer;
    word = packed array[1..10] of char;

var
    c: char;
    w: integer;
    i: integer;
    done: boolean;
    match: boolean;
    index: integer;
    numWords: integer;
    avail: letters;
    target: letters;
    foundOneAnagram: boolean;
    solution: array [1..100] of integer;
    dict: packed array [1..20] of word;


procedure solve(var target: letters;
	firstAvailWord:integer;
	numberWordsPicked:integer);
	var
		i:integer;
		j:integer;
		w:integer;
		avail: letters;

	begin {* function *}
		i:=1;
		while ((i <= 26) and (target[i] = 0)) do i := i + 1;

		if (i = 27) then begin
			{ done }
			for i := 1 to numberWordsPicked do begin
					j := 1;
			while ((dict[solution[i]][j] <> ' ') and (j <= 10)) do begin
				write(dict[solution[i]][j]);
				j := j + 1;
			end;
			write(' ');
			foundOneAnagram := true;
			end;
			writeln;
		end else begin
			for w := firstAvailWord to numWords do begin
				for i := 1 to 26 do avail[i] := target[i];
				i := 1;
				match := true;
				while ((dict[w,i] <> ' ') and (match)) do begin
					if (('a' <= dict[w,i]) and (dict[w,i] <= 'z')) then begin
						index := ord(dict[w,i]) - ord('a') + 1;
						avail[index] := avail[index] -1; 
					if (avail[index] < 0) then match := false;
					end;
				i:= i + 1;
				end;
				if (match) then begin
					solution[numberWordsPicked+1] := w;
					solve(avail,w+1,numberWordsPicked+1);
				end;
			end;
		end;
end;
			
begin
  	for i := 1 to 26 do target[i] := 0;
	while (not eoln(input)) do begin
	    read(c);
	    if ('a' <= c) and  (c <= 'z') then begin
		index := ord(c) - ord('a') + 1;
		target[index] := target[index] + 1;
	    end;
	end;
	readln;

	w := 1;
	while (not eof(input)) do begin
	    for i := 1 to 10 do dict[w,i] := ' ';
	    	i := 1;
	    	while (not eoln(input)) do begin
		 		read(dict[w][i]);
		 		i := i + 1;
	    	end;
	    	
			readln;


	    { make sure the word is possible at all }
	    for i := 1 to 26 do avail[i] := target[i];
	    match := true;
	    i := 1;
	    while ((dict[w,i] <> ' ') and (match)) do begin
			if ('a' <= dict[w,i]) and (dict[w,i] <= 'z') then begin
		    	index := ord(dict[w,i]) - ord('a') + 1;
			    avail[index] := avail[index] - 1;
		    	if (avail[index] < 0) then begin
					match := false;
		    	end;
		    	i := i + 1;
			end;
	    end;
	    if (match) then begin
			w := w + 1;
	    end;

	end;
	numWords := w;

	solve(target,1,0);
	if (not foundOneAnagram) then begin
	     writeln('No anagrams for this sentence');
	end;
end.