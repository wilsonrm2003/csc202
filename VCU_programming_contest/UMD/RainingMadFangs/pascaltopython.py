dicti = [' '] * 20
target = [0] * 26
solution =  [0] * 100
avail = [0] * 26
numWords = 0
foundOneAnagram = False
c = ' '
notEOF = True

def solve(target, firstAvailWord, numberWordsPicked):
    i = 1
    while i <= 26 and target[i] == 0:
        i += 1
    if (i == 27):
        for i in numberWordsPicked:
            j = 1
        while dicti[solution[i]][j] != ' ' and j <=10:
            print(dicti[solution[i]][j])
            j += 1
        print(" ")
        foundOneAnagram = True
        print("\n")
    else:
        w = firstAvailWord
        for w in numWords:
            for i in range(26):
                avail[i] = target[i]
                i = 1
                match = 1
                while dicti[w,i] != ' ' and match:
                    if 'a' <= dicti[w,i] and dicti[w,i] <= 'z':
                        index = ord(dicti[w,i]) - ord('a') + 1
                        avail[index] = avail[index] - 1
                    if avail[index] < 0:
                        match = False
                    i += 1
                if (match):
                    solution[numberWordsPicked] = w
                    solve(avail, w+1, numberWordsPicked+1)

def main():
    for i in range(26):
        target[i] = 0
    while not eoln(input()): #look up eoln and EOF
        read(c)
        if 'a' <= c and c <= 'z':
            index = ord(c) - ord('a') + 1
            target[index] = target[index] + 1 
    w = 1 
    while notEOF:
        line = input()
        if line == '':
            notEOF = False
        for i in range(1,10):
            dicti[w,i] = ' '
        i = 1
        while not eoln(input):

            readline(dicti[w][i])
            i += 1

        for i in range(26):
            avail[i] = target[i]
        match = True
        i = 1
        while dicti[w,i] != ' ' and match:
            if 'a' <= dicti[w,i] and dicti[w,i] <= 'z':
                index = ord(dicti[w,i]) - ord('a') + 1
                avail[index] = avail[index] - 1
                if avail[index] < 0:
                    match = False
                i += 1
        if match:
            w += 1
    numWords = w

    solve(target, 1, 0)
    if foundOneAnagram == False:
        print("\n No anagrams for this sentence")